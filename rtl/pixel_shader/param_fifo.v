
 //-----------------------------------------------------------------------------
//
//Copyright(c) 2020, ThorsianWay Technologies Co, Ltd
//All rights reserved.
//
//IP Name       :   pixel_shader
//File Name     :   param_fifo.v
//Module name   :   param_fifo
//Full name     :   parameter fifo for bilinear interpolaration 
//
//Author        :   zha daolu
//Email         :   
//Data          :   2020/5/13
//Version       :   V1.00
//
//Abstract      :   
//                  
//Called  by    :   GPU
//
//Modification history
//-----------------------------------------------------
//1.00: intial version 
//
//-----------------------------------------------------------------------------   

module param_fifo(
    SCAN_mode                  ,
    rst_n                      ,  //input reset, low active                                      
    clk                        ,  //input clock                                                  
    param1                     ,  //bilinear parameter1                                          
    param2                     ,  //bilinear parameter2                                          
    param3                     ,  //bilinear parameter3                                          
    param4                     ,  //bilinear parameter4                                          
    param_fifo_w_en            ,  //parameter fifo write enable                                  
    param_en                   ,  //parameter valid                                              
    param_fifo_almost_full     ,  //parameter fifo almost full                                   
    addr_fifo_w_en             ,  //texel address valid, 1: bilinear enable, 0: address valid    
    param_en_out               ,  //parameter fifo output parameter valid                        
    param1_out                 ,  //parameter fifo output parameter 1                            
    param2_out                 ,  //parameter fifo output parameter 2                            
    param3_out                 ,  //parameter fifo output parameter 3                            
    param4_out                 ,  //parameter fifo output parameter 4                            
    param_fifo_rd                 //parameter fifo read                                          
);
input SCAN_mode                  ;
input rst_n                      ;
input clk                        ;
input [7:0] param1               ;
input [7:0] param2               ;
input [7:0] param3               ;
input [7:0] param4               ;
input param_fifo_w_en            ;
input param_en                   ;
input [1:0]       addr_fifo_w_en ;
output param_fifo_almost_full    ;
output param_en_out              ;
output [7:0]param1_out           ;
output [7:0]param2_out           ;
output [7:0]param3_out           ;
output [7:0]param4_out           ;
input param_fifo_rd              ;

wire param_en_fifo_almost_full;
wire parameter_fifo_almost_full;
assign param_fifo_almost_full = param_en_fifo_almost_full | parameter_fifo_almost_full;

//parameter valid fifo
fifo #(
    .DWIDTH(1),
    .DSIZE(8)//8
)u_param_en_fifo(
    .SCAN_mode(1'b0),
    .datain(addr_fifo_w_en[1]), 
    .rd(param_fifo_rd), 
    .wr(addr_fifo_w_en[0]), 
    .rst_n(rst_n), 
    .clk(clk), 
    .dataout(param_en_out), 
    .full(), 
    .empty(),
    .almost_full(param_en_fifo_almost_full)
);

//parameter data fifo
fifo #(
    .DWIDTH(32),
    .DSIZE(6)//6
)u_parameter_fifo(
    .SCAN_mode(SCAN_mode),
    .datain({param1,param2,param3,param4}), 
    .rd(param_fifo_rd && param_en_out), 
    .wr(param_fifo_w_en && param_en), 
    .rst_n(rst_n), 
    .clk(clk), 
    .dataout({param1_out,param2_out,param3_out,param4_out}), 
    .full(), 
    .empty(),
    .almost_full(parameter_fifo_almost_full)
);

endmodule
