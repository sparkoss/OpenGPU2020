module decoder #(
parameter       SYSTEM_ADDR_BITS = 32,
parameter       MEMORY_DATA_WIDTH_LOG2 = 4
)
(
        input                                           clk,
        input                                           rst_n,
        input                                           busy,
        input      [1:0]               texture_block_addr,
        input      [3:0]                                textype,
        input      [6:0]                                in_texture_addr,
        //input                                           rd_en_ff1,
        input                                           l1_out_en,//cache_busy,
        input      [255:0]  l1_dout,
        output     [23:0]  color,
        output     [7:0]   alpha,
        output reg         outen
);


localparam   DXT1    = 4'b0000;
localparam   DXT3    = 4'b0001;
localparam   ALPHA1  = 4'b0010;
localparam   ALPHA8  = 4'b1010;
localparam   RGB565  = 4'b0100;
localparam   RGB8888 = 4'b0101;
localparam   Y8      = 4'b0110;
localparam   RGB565_RE  = 4'b0111;
localparam   DXT5    = 4'he;
localparam   RGB8888_RE = 4'hf;

localparam ALPHA2 = 4'b1001;
localparam ALPHA4 = 4'b1011;
localparam BMP1555 = 4'b1100;
localparam BMP4444 = 4'b1101;

reg  [7:0]  data8;
reg  [7:0]  dxt3_alpha;
wire [7:0]  dxt5_alpha;
reg  [15:0] data16;
reg  [31:0] data32;
reg  [7:0]  mask8bit;
wire [23:0] dxt_dout;
reg  [1:0]  alpha2bit;
reg  [3:0]  alpha4bit;

reg [63:0] dout;
reg [63:0] dxt3_alpha_dout;
reg  [6:0] in_texture_addr_ff1;
//reg  [6:0] in_texture_addr_ff2;
reg l1_out_en_ff1;
reg l1_out_en_ff2;
reg l1_out_en_ff3;
reg l1_out_en_ff4;

reg [7:0]    alpha_in;
reg [23:0]   color_in;

reg [7:0]    alpha_in_ff;
reg [23:0]   color_in_ff;

wire[127:0]    l1_dout_128 = texture_block_addr[1] ? l1_dout[255:128] : l1_dout[127:0]; 

always @(posedge clk or negedge rst_n)
begin
        if(~rst_n)
        begin
                l1_out_en_ff1 <=  0;
                l1_out_en_ff2 <=  0;
                l1_out_en_ff3 <=  0;
                l1_out_en_ff4 <=  0;
        end
        else if(~busy)
        begin
                l1_out_en_ff1 <=  l1_out_en;
                l1_out_en_ff2 <=  l1_out_en_ff1;
                l1_out_en_ff3 <=  l1_out_en_ff2;
                l1_out_en_ff4 <=  l1_out_en_ff3;
        end
end

//stage 0

always @(*)
begin
        dxt3_alpha_dout = l1_dout_128[63:0];
        if(textype == DXT3 || textype == DXT5) dout = l1_dout_128[127:64];
        else dout = (texture_block_addr[0]) ? l1_dout_128[127:64] : l1_dout_128[63:0];          
end


//stage 1



always @(posedge clk or negedge rst_n)
begin
        if(~rst_n) data8 <=  0;
        else if(l1_out_en && ~busy)
                case(in_texture_addr[5:3])
                        0: data8 <=  dout[7:0];
                        1: data8 <=  dout[15:8];
                        2: data8 <=  dout[23:16];
                        3: data8 <=  dout[31:24];
                        4: data8 <=  dout[39:32];
                        5: data8 <=  dout[47:40];
                        6: data8 <=  dout[55:48];
                        7: data8 <=  dout[63:56];
                endcase
end

always @(posedge clk or negedge rst_n)
begin
        if(~rst_n) data16 <=  0;
        else if(l1_out_en  && ~busy)
                case(in_texture_addr[5:4])
                        0: data16 <=  dout[15:0];
                        1: data16 <=  dout[31:16];
                        2: data16 <=  dout[47:32];
                        3: data16 <=  dout[63:48];
                endcase
end

always @(posedge clk or negedge rst_n)
begin
        if(~rst_n) data32 <=  0;
        else if(l1_out_en && ~busy)
                case(in_texture_addr[5])
                        0: data32 <=  dout[31:0];
                        1: data32 <=  dout[63:32];
                endcase
end


always @(posedge clk or negedge rst_n)
begin
        if(~rst_n) dxt3_alpha <=  0;
        else if(l1_out_en && ~busy)
                case(in_texture_addr[3:0])
                        4'h0: dxt3_alpha <=  {2{dxt3_alpha_dout[3:0]  }};
                        4'h1: dxt3_alpha <=  {2{dxt3_alpha_dout[7:4]  }};
                        4'h2: dxt3_alpha <=  {2{dxt3_alpha_dout[11:8] }};
                        4'h3: dxt3_alpha <=  {2{dxt3_alpha_dout[15:12]}};
                        4'h4: dxt3_alpha <=  {2{dxt3_alpha_dout[19:16]}};
                        4'h5: dxt3_alpha <=  {2{dxt3_alpha_dout[23:20]}};
                        4'h6: dxt3_alpha <=  {2{dxt3_alpha_dout[27:24]}};
                        4'h7: dxt3_alpha <=  {2{dxt3_alpha_dout[31:28]}};
                        4'h8: dxt3_alpha <=  {2{dxt3_alpha_dout[35:32]}};
                        4'h9: dxt3_alpha <=  {2{dxt3_alpha_dout[39:36]}};
                        4'ha: dxt3_alpha <=  {2{dxt3_alpha_dout[43:40]}};
                        4'hb: dxt3_alpha <=  {2{dxt3_alpha_dout[47:44]}};
                        4'hc: dxt3_alpha <=  {2{dxt3_alpha_dout[51:48]}};
                        4'hd: dxt3_alpha <=  {2{dxt3_alpha_dout[55:52]}};
                        4'he: dxt3_alpha <=  {2{dxt3_alpha_dout[59:56]}};
                        4'hf: dxt3_alpha <=  {2{dxt3_alpha_dout[63:60]}};            
                endcase
end


always @(posedge clk or negedge rst_n)
begin
        if(~rst_n)
        begin
                in_texture_addr_ff1 <=  0;
                //in_texture_addr_ff2 <=  0;
        end
        else if (l1_out_en && ~busy) 
        begin
                in_texture_addr_ff1 <=  in_texture_addr;
                //in_texture_addr_ff2 <=  in_texture_addr_ff1;
        end
end

dxt_decoder dxt_decoder(
        .clk            (clk      ),
        .rst_n          (rst_n    ),
        .rd_en_ff2      (l1_out_en),
        .cache_busy     (1'b0),
        .l1_dout        (dout)  ,
        .dxt_texel      (dxt_dout)  ,
        .in_texture_addr_ff2(in_texture_addr[3:0])
);


alpha_decoder uut_alpha_decoder(
        .clk                 (clk)  ,
        .rst_n               (rst_n)  ,
        .rd_en               (l1_out_en)  ,
        .cache_busy          (1'b0)  ,
    .l1_dout             (dxt3_alpha_dout)  ,
    .alpha               (dxt5_alpha)  ,
        .in_texture_addr     (in_texture_addr[3:0])
);


//stage 2

always @(*)
begin
        case(in_texture_addr_ff1[2:1])
                0: alpha2bit = data8[1:0];
                1: alpha2bit = data8[3:2];
                2: alpha2bit = data8[5:4];
                3: alpha2bit = data8[7:6];
        endcase
end

always @(*)
begin
        case(in_texture_addr_ff1[2])
                0: alpha4bit = data8[3:0];
                1: alpha4bit = data8[7:4];
        endcase
end


always @(posedge clk or negedge rst_n)
begin
    if(~rst_n) alpha_in <=  0;
        else if (l1_out_en_ff1 && ~busy) 
        begin
                if(in_texture_addr_ff1[6])
                        alpha_in <=  0;
                else
                begin
                        case(textype)
                                ALPHA8:   alpha_in <=  data8;
                                RGB8888:  alpha_in <=  data32[31:24];
                RGB8888_RE:  alpha_in <=  data32[31:24];   
                                ALPHA1:   alpha_in <=  {8{data8[in_texture_addr_ff1[2:0]]}};
                                ALPHA2:   alpha_in <=  {4{alpha2bit}};
                                ALPHA4:   alpha_in <=  {2{alpha4bit}};
                                DXT3:     alpha_in <=  dxt3_alpha;
                BMP1555:  alpha_in <=  {8{data16[15]}};
                BMP4444:  alpha_in <=  {2{data16[15:12]}};
                                default:  alpha_in <=  8'hff;
                        endcase
                end
        end
end

always @(posedge clk or negedge rst_n)
begin
        if(~rst_n) color_in <=  0;
        else if (l1_out_en_ff1 && ~busy) 
        begin
                if(in_texture_addr_ff1[6])
                        color_in <=  0;
                else
                begin
                        case(textype)
                                RGB8888: color_in <=  data32[23:0];
                RGB8888_RE: color_in <=  data32[23:0];
                RGB565_RE: color_in <=  {data16[15:11],data16[15:13],data16[10:5],data16[10:9],data16[4:0],data16[4:2]};
                                Y8: color_in <=  {data8,data8,data8};
                                DXT1: color_in <=  dxt_dout;
                                DXT3: color_in <=  dxt_dout;
                                DXT5: color_in <=  dxt_dout;
                                RGB565:
                                        color_in <=  {data16[15:11],data16[15:13],data16[10:5],data16[10:9],data16[4:0],data16[4:2]};//RGB565
                BMP1555: color_in <=  {data16[14:10],data16[14:12],data16[9:5],data16[9:7],data16[4:0],data16[4:2]};
                BMP4444: color_in <=  {{2{data16[11:8]}},{2{data16[7:4]}},{2{data16[3:0]}}};
                        default:  color_in <=  0;//{data16[15:11],data16[15:13],data16[10:5],data16[10:9],data16[4:0],data16[4:2]};//RGB565
                        endcase
                end
        end
end

//stage 3




always @(posedge clk or negedge rst_n)
begin
        if(~rst_n)
        begin
                alpha_in_ff <=  0;
                color_in_ff <=  0;
        end
        else if (l1_out_en_ff2 && ~busy) 
        begin
                alpha_in_ff <=  alpha_in;
                color_in_ff <=  color_in;
        end
end

//stage 4
assign alpha = (textype == DXT5) ? dxt5_alpha : alpha_in_ff;
assign color = color_in_ff;


always @(posedge clk or negedge rst_n)
begin
        if(~rst_n) outen <=  0;
        else if(~busy) outen <=  l1_out_en_ff2;// && ~busy;
end






endmodule

