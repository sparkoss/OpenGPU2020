 //-----------------------------------------------------------------------------
//
//Copyright(c) 2020, ThorsianWay Technologies Co, Ltd
//All rights reserved.
//
//IP Name       :   pixel_shader
//File Name     :   mul_8x8.v
//Module name   :   mul_8x8
//Full name     :   8x8 mulitplier 
//
//Author        :   zha daolu
//Email         :   
//Data          :   2020/5/13
//Version       :   V1.00
//
//Abstract      :   
//                  
//Called  by    :   GPU
//
//Modification history
//-----------------------------------------------------
//1.00: intial version 
//
//-----------------------------------------------------------------------------   
module mult_8x8(
    input [7:0] a,
    input [7:0] b,
    output [15:0] out
);
assign out = a*b;
endmodule
