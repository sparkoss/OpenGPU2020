//-----------------------------------------------------------------------------
//
//Copyright(c) 2020, ThorsianWay Technologies Co, Ltd
//All rights reserved.
//
//IP Name       :   raster
//File Name     :   fragment_gen_smooth.v
//Module name   :   fragment_gen_smooth
//Full name     :   generate fragment for smooth mode
//
//Author        :   zha daolu
//Email         :   
//Data          :   2020/6/1
//Version       :   V1.00
//
//Abstract      :   
//                  
//Called  by    :   GPU
//
//Modification history
//-----------------------------------------------------
//1.00: intial version 
//
//-----------------------------------------------------------------------------

//-----------------------------
//DEFINE MACRO
//-----------------------------  
module fragment_gen_smooth(
    input clk,                                             //input clock              
    input rst_n,                                           //input reset, low active  
    input busy,                                            //input busy               
    input [31:0] vertex2_z               ,                 //input vertex2 depth
    input [31:0] vertex2_w               ,                 //input vertex2 1/w  , float
    input signed [31:0] vertex2_s              ,           //input vertex2 s/w  , float
    input signed [31:0] vertex2_t              ,           //input vertex2 t/w  , float
    input [7:0]  vertex2_primary_r       ,                 //input vertex2 color red channel     
    input [7:0]  vertex2_primary_g      ,                  //input vertex2 color green channel   
    input [7:0]  vertex2_primary_b      ,                  //input vertex2 color blue channel    
    input [7:0]  vertex2_primary_a      ,                  //input vertex2 color alpha channel   
    input [95:0] ee0,                                      //input pixel edge function 0 
    input [95:0] ee1,                                      //input pixel edge function 1 
    input [95:0] ee2,                                      //input pixel edge function 2 
    input signed[31:0] dz_02                        ,      //input differnce of z0 , z2
    input signed[31:0] dw_02                        ,      //input 1/w1  - 1/w2
    input signed[8:0]  dprimary_r_02                ,      //input differnce of r0 , r2
    input signed[8:0]  dprimary_g_02                ,      //input differnce of g0 , g2
    input signed[8:0]  dprimary_b_02                ,      //input differnce of b0 , b2
    input signed[8:0]  dprimary_a_02                ,      //input differnce of a0 , a2
    input signed[31:0] dz_12                        ,      //input differnce of z1 , z2
    input signed[31:0] dw_12                        ,      //input 1/w0  - 1/w2 
    input signed[31:0] ds_02                       ,       //input s0/w0 - s2/w2
    input signed[31:0] dt_02                       ,       //input t0/w0 - t2/w2
    input signed[31:0] ds_12                       ,       //input s1/w1 - s2/w2  
    input signed[31:0] dt_12                       ,       //input t1/w1 - t2/w2 
    input signed[8:0]  dprimary_r_12                ,      //input differnce of r1 , r2
    input signed[8:0]  dprimary_g_12                ,      //input differnce of g1 , g2
    input signed[8:0]  dprimary_b_12                ,      //input differnce of b1 , b2
    input signed[8:0]  dprimary_a_12                ,      //input differnce of a1 , a2
    input signed[95:0] area                         ,      //input triangle area x 2
    input [31:0]                    x_in,                  //input overlap x coordinate
    input [31:0]                    y_in,                  //input overlap y coordinate
    input                         xy_in_en,                  //input overlap pixel valid 
    input                             mode_end,              //input bte end
    output [31:0]                     x_out,                //output x coordinate 
    output [31:0]                     y_out,                //output y coordinate
    output [31:0]                    z_out,                //output depth
    output                       xy_out_en,                 //output fragment valid
    output [7:0]  primary_r                ,               //output fragment color red channel    
    output [7:0]  primary_g                ,               //output fragment color green channel  
    output [7:0]  primary_b                ,               //output fragment color blue channel   
    output [7:0]  primary_a                ,               //output fragment color alpha channel  
    output [31:0] s                       ,                //output s coordinate
    output [31:0] t                       ,                //output t coordinate
    output reg fragment_gen_end           ,                //output fragment gen end
    input [31:0] z_slope,                                  //depth slope
    input        depth_offset_enable,                      //depth offset enable
    input [31:0] factor,                                   //depth offset factor
    input [31:0] unit                                      //depth offset unit
);
//*****************************************
//first stage,8 cycle    
// calculate interpolate paramater k1, k2
// k1 = ee1/area; k2 = ee2/area;
//*****************************************

//registered input data
reg signed [95:0] ee0_ff1;
reg signed [95:0] ee1_ff1;  
reg signed [95:0] ee2_ff1;
reg signed [95:0] ee0_ff2;
reg signed [95:0] ee1_ff2;  
reg signed [95:0] ee2_ff2;  
reg [95:0] area_ff1;
reg [95:0] area_ff2;  
reg signed [47:0] vertex2_z_ff1               ;
reg signed [31:0] vertex2_w_ff1               ;
reg signed [31:0] vertex2_s_ff1              ;
reg signed [31:0] vertex2_t_ff1              ;
reg [7:0]  vertex2_primary_r_ff1       ;
reg [7:0]  vertex2_primary_g_ff1      ;
reg [7:0]  vertex2_primary_b_ff1      ;
reg [7:0]  vertex2_primary_a_ff1      ;
reg signed[47:0] dz_02_ff1                        ;
reg signed[31:0] dw_02_ff1                        ;
reg signed[31:0] ds_02_ff1                       ;
reg signed[31:0] dt_02_ff1                       ; 
reg signed[8:0]  dprimary_r_02_ff1                ;
reg signed[8:0]  dprimary_g_02_ff1                ;
reg signed[8:0]  dprimary_b_02_ff1                ;
reg signed[8:0]  dprimary_a_02_ff1                ;
reg signed[47:0] dz_12_ff1                        ;
reg signed[31:0] dw_12_ff1                        ;
reg signed[31:0] ds_12_ff1                       ;
reg signed[31:0] dt_12_ff1                       ;
reg signed[8:0]  dprimary_r_12_ff1                ;
reg signed[8:0]  dprimary_g_12_ff1                ;
reg signed[8:0]  dprimary_b_12_ff1                ;
reg signed[8:0]  dprimary_a_12_ff1                ;
reg [31:0]         x_in_ff1;
reg [31:0]         y_in_ff1;

reg signed [47:0] vertex2_z_ff2               ;
reg signed [31:0] vertex2_w_ff2               ;
reg signed [31:0] vertex2_s_ff2              ;
reg signed [31:0] vertex2_t_ff2              ;
reg [7:0]  vertex2_primary_r_ff2       ;
reg [7:0]  vertex2_primary_g_ff2      ;
reg [7:0]  vertex2_primary_b_ff2      ;
reg [7:0]  vertex2_primary_a_ff2      ;
reg signed[47:0] dz_02_ff2                        ;
reg signed[31:0] dw_02_ff2                        ;
reg signed[8:0]  dprimary_r_02_ff2                ;
reg signed[8:0]  dprimary_g_02_ff2                ;
reg signed[8:0]  dprimary_b_02_ff2                ;
reg signed[8:0]  dprimary_a_02_ff2                ;
reg signed[47:0] dz_12_ff2                        ;
reg signed[31:0] dw_12_ff2                        ;
reg signed[31:0] ds_02_ff2                       ;
reg signed[31:0] dt_02_ff2                       ; 
reg signed[31:0] ds_12_ff2                       ;
reg signed[31:0] dt_12_ff2                       ;
reg signed[8:0]  dprimary_r_12_ff2                ;
reg signed[8:0]  dprimary_g_12_ff2                ;
reg signed[8:0]  dprimary_b_12_ff2                ;
reg signed[8:0]  dprimary_a_12_ff2                ;
reg [31:0]         x_in_ff2;
reg [31:0]         y_in_ff2;


reg signed [47:0] vertex2_z_ff3               ;
reg signed [31:0] vertex2_w_ff3               ;
reg signed [31:0] vertex2_s_ff3              ;
reg signed [31:0] vertex2_t_ff3              ;
reg [7:0]  vertex2_primary_r_ff3       ;
reg [7:0]  vertex2_primary_g_ff3      ;
reg [7:0]  vertex2_primary_b_ff3      ;
reg [7:0]  vertex2_primary_a_ff3      ;
reg signed[47:0] dz_02_ff3                        ;
reg signed[31:0] dw_02_ff3                        ;
reg signed[8:0]  dprimary_r_02_ff3                ;
reg signed[8:0]  dprimary_g_02_ff3                ;
reg signed[8:0]  dprimary_b_02_ff3                ;
reg signed[8:0]  dprimary_a_02_ff3                ;
reg signed[47:0] dz_12_ff3                        ;
reg signed[31:0] dw_12_ff3                        ;
reg signed[31:0] ds_02_ff3                       ;
reg signed[31:0] dt_02_ff3                       ; 
reg signed[31:0] ds_12_ff3                       ;
reg signed[31:0] dt_12_ff3                       ;
reg signed[8:0]  dprimary_r_12_ff3                ;
reg signed[8:0]  dprimary_g_12_ff3                ;
reg signed[8:0]  dprimary_b_12_ff3                ;
reg signed[8:0]  dprimary_a_12_ff3                ;
reg [31:0]         x_in_ff3;
reg [31:0]         y_in_ff3;

reg signed [47:0] vertex2_z_ff4               ;
reg signed [31:0] vertex2_w_ff4               ;
reg signed [31:0] vertex2_s_ff4              ;
reg signed [31:0] vertex2_t_ff4              ;
reg [7:0]  vertex2_primary_r_ff4       ;
reg [7:0]  vertex2_primary_g_ff4      ;
reg [7:0]  vertex2_primary_b_ff4      ;
reg [7:0]  vertex2_primary_a_ff4      ;
reg signed[47:0] dz_02_ff4                        ;
reg signed[31:0] dw_02_ff4                        ;
reg signed[8:0]  dprimary_r_02_ff4                ;
reg signed[8:0]  dprimary_g_02_ff4                ;
reg signed[8:0]  dprimary_b_02_ff4                ;
reg signed[8:0]  dprimary_a_02_ff4                ;
reg signed[47:0] dz_12_ff4                        ;
reg signed[31:0] dw_12_ff4                        ;
reg signed[31:0] ds_02_ff4                       ;
reg signed[31:0] dt_02_ff4                       ;
reg signed[31:0] ds_12_ff4                       ;
reg signed[31:0] dt_12_ff4                       ;
reg signed[8:0]  dprimary_r_12_ff4                ;
reg signed[8:0]  dprimary_g_12_ff4                ;
reg signed[8:0]  dprimary_b_12_ff4                ;
reg signed[8:0]  dprimary_a_12_ff4                ;
reg [31:0]         x_in_ff4;
reg [31:0]         y_in_ff4;


reg signed [47:0] vertex2_z_ff5               ;
reg signed [31:0] vertex2_w_ff5               ;
reg signed [31:0] vertex2_s_ff5              ;
reg signed [31:0] vertex2_t_ff5              ;
reg [7:0]  vertex2_primary_r_ff5       ;
reg [7:0]  vertex2_primary_g_ff5      ;
reg [7:0]  vertex2_primary_b_ff5      ;
reg [7:0]  vertex2_primary_a_ff5      ;
reg signed[47:0] dz_02_ff5                        ;
reg signed[31:0] dw_02_ff5                        ;
reg signed[8:0]  dprimary_r_02_ff5                ;
reg signed[8:0]  dprimary_g_02_ff5                ;
reg signed[8:0]  dprimary_b_02_ff5                ;
reg signed[8:0]  dprimary_a_02_ff5                ;
reg signed[47:0] dz_12_ff5                        ;
reg signed[31:0] dw_12_ff5                        ;
reg signed[31:0] ds_02_ff5                       ;
reg signed[31:0] dt_02_ff5                       ; 
reg signed[31:0] ds_12_ff5                       ;
reg signed[31:0] dt_12_ff5                       ;
reg signed[8:0]  dprimary_r_12_ff5                ;
reg signed[8:0]  dprimary_g_12_ff5                ;
reg signed[8:0]  dprimary_b_12_ff5                ;
reg signed[8:0]  dprimary_a_12_ff5                ;
reg [31:0]         x_in_ff5;
reg [31:0]         y_in_ff5;

reg signed [47:0] vertex2_z_ff6               ;
reg signed [31:0] vertex2_w_ff6               ;
reg signed [31:0] vertex2_s_ff6              ;
reg signed [31:0] vertex2_t_ff6              ;
reg [7:0]  vertex2_primary_r_ff6       ;
reg [7:0]  vertex2_primary_g_ff6      ;
reg [7:0]  vertex2_primary_b_ff6      ;
reg [7:0]  vertex2_primary_a_ff6      ;
reg signed[47:0] dz_02_ff6                        ;
reg signed[31:0] dw_02_ff6                        ;
reg signed[8:0]  dprimary_r_02_ff6                ;
reg signed[8:0]  dprimary_g_02_ff6                ;
reg signed[8:0]  dprimary_b_02_ff6                ;
reg signed[8:0]  dprimary_a_02_ff6                ;
reg signed[47:0] dz_12_ff6                        ;
reg signed[31:0] dw_12_ff6                        ;
reg signed[31:0] ds_02_ff6                       ;
reg signed[31:0] dt_02_ff6                       ; 
reg signed[31:0] ds_12_ff6                       ;
reg signed[31:0] dt_12_ff6                       ;
reg signed[8:0]  dprimary_r_12_ff6                ;
reg signed[8:0]  dprimary_g_12_ff6                ;
reg signed[8:0]  dprimary_b_12_ff6                ;
reg signed[8:0]  dprimary_a_12_ff6                ;
reg [31:0]         x_in_ff6;
reg [31:0]         y_in_ff6;

reg signed [47:0] vertex2_z_ff7               ;
reg signed [31:0] vertex2_w_ff7               ;
reg signed [31:0] vertex2_s_ff7              ;
reg signed [31:0] vertex2_t_ff7              ;
reg [7:0]  vertex2_primary_r_ff7       ;
reg [7:0]  vertex2_primary_g_ff7      ;
reg [7:0]  vertex2_primary_b_ff7      ;
reg [7:0]  vertex2_primary_a_ff7      ;
reg signed[47:0] dz_02_ff7                        ;
reg signed[31:0] dw_02_ff7                        ;
reg signed[8:0]  dprimary_r_02_ff7                ;
reg signed[8:0]  dprimary_g_02_ff7                ;
reg signed[8:0]  dprimary_b_02_ff7                ;
reg signed[8:0]  dprimary_a_02_ff7                ;
reg signed[47:0] dz_12_ff7                        ;
reg signed[31:0] dw_12_ff7                        ;
reg signed[31:0] ds_02_ff7                       ;
reg signed[31:0] dt_02_ff7                       ; 
reg signed[31:0] ds_12_ff7                       ;
reg signed[31:0] dt_12_ff7                       ;
reg signed[8:0]  dprimary_r_12_ff7                ;
reg signed[8:0]  dprimary_g_12_ff7                ;
reg signed[8:0]  dprimary_b_12_ff7                ;
reg signed[8:0]  dprimary_a_12_ff7                ;
reg [31:0]         x_in_ff7;
reg [31:0]         y_in_ff7;

reg signed [47:0] vertex2_z_ff8               ;
reg signed [31:0] vertex2_w_ff8               ;
reg signed [31:0] vertex2_s_ff8              ;
reg signed [31:0] vertex2_t_ff8              ;
reg [7:0]  vertex2_primary_r_ff8       ;
reg [7:0]  vertex2_primary_g_ff8      ;
reg [7:0]  vertex2_primary_b_ff8      ;
reg [7:0]  vertex2_primary_a_ff8      ;
reg signed[47:0] dz_02_ff8                        ;
reg signed[31:0] dw_02_ff8                        ;
reg signed[8:0]  dprimary_r_02_ff8                ;
reg signed[8:0]  dprimary_g_02_ff8                ;
reg signed[8:0]  dprimary_b_02_ff8                ;
reg signed[8:0]  dprimary_a_02_ff8                ;
reg signed[47:0] dz_12_ff8                        ;
reg signed[31:0] dw_12_ff8                        ;
reg signed[31:0] ds_02_ff8                       ;
reg signed[31:0] dt_02_ff8                       ; 
reg signed[31:0] ds_12_ff8                       ;
reg signed[31:0] dt_12_ff8                       ;
reg signed[8:0]  dprimary_r_12_ff8                ;
reg signed[8:0]  dprimary_g_12_ff8                ;
reg signed[8:0]  dprimary_b_12_ff8                ;
reg signed[8:0]  dprimary_a_12_ff8                ;
reg [31:0]         x_in_ff8;
reg [31:0]         y_in_ff8;

reg signed [47:0] vertex2_z_ff9               ;
reg signed [31:0] vertex2_w_ff9               ;
reg signed [31:0] vertex2_s_ff9              ;
reg signed [31:0] vertex2_t_ff9              ;
reg [7:0]  vertex2_primary_r_ff9       ;
reg [7:0]  vertex2_primary_g_ff9      ;
reg [7:0]  vertex2_primary_b_ff9      ;
reg [7:0]  vertex2_primary_a_ff9      ;
reg signed[47:0] dz_02_ff9                        ;
reg signed[31:0] dw_02_ff9                        ;
reg signed[8:0]  dprimary_r_02_ff9                ;
reg signed[8:0]  dprimary_g_02_ff9                ;
reg signed[8:0]  dprimary_b_02_ff9                ;
reg signed[8:0]  dprimary_a_02_ff9                ;
reg signed[47:0] dz_12_ff9                        ;
reg signed[31:0] dw_12_ff9                        ;
reg signed[31:0] ds_02_ff9                       ;
reg signed[31:0] dt_02_ff9                       ; 
reg signed[31:0] ds_12_ff9                       ;
reg signed[31:0] dt_12_ff9                       ;
reg signed[8:0]  dprimary_r_12_ff9                ;
reg signed[8:0]  dprimary_g_12_ff9                ;
reg signed[8:0]  dprimary_b_12_ff9                ;
reg signed[8:0]  dprimary_a_12_ff9                ;
reg [31:0]         x_in_ff9;
reg [31:0]         y_in_ff9;
                             


always@(posedge clk or negedge rst_n)
begin
    if(~rst_n)
    begin
        ee0_ff1                   <= 96'b0;
        ee1_ff1                   <= 96'b0;
        ee2_ff1                   <= 96'b0;
        ee0_ff2                   <= 96'b0;
        ee1_ff2                   <= 96'b0;
        ee2_ff2                   <= 96'b0; 
        area_ff1                  <= 96'b0;
        area_ff2                  <= 96'b0; 
        vertex2_z_ff1             <= 48'b0;   
        vertex2_w_ff1             <= 32'b0;   
        vertex2_s_ff1             <= 32'b0;   
        vertex2_t_ff1             <= 32'b0;   
        vertex2_primary_r_ff1     <=  8'b0;   
        vertex2_primary_g_ff1     <=  8'b0;  
        vertex2_primary_b_ff1     <=  8'b0;  
        vertex2_primary_a_ff1     <=  8'b0;  
        dz_02_ff1                 <= 48'b0;        
        dw_02_ff1                 <= 32'b0;
        ds_02_ff1                 <= 32'b0;        
        dt_02_ff1                 <= 32'b0;  
        dprimary_r_02_ff1         <= 9'b0;        
        dprimary_g_02_ff1         <= 9'b0;        
        dprimary_b_02_ff1         <= 9'b0;        
        dprimary_a_02_ff1         <= 9'b0;        
        dz_12_ff1                 <= 48'b0;        
        dw_12_ff1                 <= 32'b0;        
        ds_12_ff1                 <= 32'b0;        
        dt_12_ff1                 <= 32'b0;        
        dprimary_r_12_ff1         <=  8'b0;        
        dprimary_g_12_ff1         <=  8'b0;        
        dprimary_b_12_ff1         <=  8'b0;        
        dprimary_a_12_ff1         <=  8'b0;        
        x_in_ff1                  <= 32'b0;
        y_in_ff1                  <= 32'b0;
        vertex2_z_ff2             <= 48'b0;   
        vertex2_w_ff2             <= 32'b0;   
        vertex2_s_ff2             <= 32'b0;   
        vertex2_t_ff2             <= 32'b0;   
        vertex2_primary_r_ff2     <=  8'b0;   
        vertex2_primary_g_ff2     <=  8'b0;  
        vertex2_primary_b_ff2     <=  8'b0;  
        vertex2_primary_a_ff2     <=  8'b0;  
        dz_02_ff2                 <= 48'b0;        
        dw_02_ff2                 <= 32'b0;
        ds_02_ff2                 <= 32'b0;        
        dt_02_ff2                 <= 32'b0;  
        dprimary_r_02_ff2         <= 9'b0;        
        dprimary_g_02_ff2         <= 9'b0;        
        dprimary_b_02_ff2         <= 9'b0;        
        dprimary_a_02_ff2         <= 9'b0;        
        dz_12_ff2                 <= 48'b0;        
        dw_12_ff2                 <= 32'b0;        
        ds_12_ff2                 <= 32'b0;        
        dt_12_ff2                 <= 32'b0;        
        dprimary_r_12_ff2         <=  8'b0;        
        dprimary_g_12_ff2         <=  8'b0;        
        dprimary_b_12_ff2         <=  8'b0;        
        dprimary_a_12_ff2         <=  8'b0;        
        x_in_ff2                  <= 32'b0;
        y_in_ff2                  <= 32'b0; 
        vertex2_z_ff3             <= 48'b0;   
        vertex2_w_ff3             <= 32'b0;   
        vertex2_s_ff3             <= 32'b0;   
        vertex2_t_ff3             <= 32'b0;   
        vertex2_primary_r_ff3     <=  8'b0;   
        vertex2_primary_g_ff3     <=  8'b0;  
        vertex2_primary_b_ff3     <=  8'b0;  
        vertex2_primary_a_ff3     <=  8'b0;  
        dz_02_ff3                 <= 48'b0;        
        dw_02_ff3                 <= 32'b0;
        ds_02_ff3                 <= 32'b0;        
        dt_02_ff3                 <= 32'b0;  
        dprimary_r_02_ff3         <= 9'b0;        
        dprimary_g_02_ff3         <= 9'b0;        
        dprimary_b_02_ff3         <= 9'b0;        
        dprimary_a_02_ff3         <= 9'b0;        
        dz_12_ff3                 <= 48'b0;        
        dw_12_ff3                 <= 32'b0;        
        ds_12_ff3                 <= 32'b0;        
        dt_12_ff3                 <= 32'b0;        
        dprimary_r_12_ff3         <=  8'b0;        
        dprimary_g_12_ff3         <=  8'b0;        
        dprimary_b_12_ff3         <=  8'b0;        
        dprimary_a_12_ff3         <=  8'b0;        
        x_in_ff3                  <= 32'b0;
        y_in_ff3                  <= 32'b0; 
        vertex2_z_ff4             <= 48'b0;   
        vertex2_w_ff4             <= 32'b0;   
        vertex2_s_ff4             <= 32'b0;   
        vertex2_t_ff4             <= 32'b0;   
        vertex2_primary_r_ff4     <=  8'b0;   
        vertex2_primary_g_ff4     <=  8'b0;  
        vertex2_primary_b_ff4     <=  8'b0;  
        vertex2_primary_a_ff4     <=  8'b0;  
        dz_02_ff4                 <= 48'b0;        
        dw_02_ff4                 <= 32'b0;
        ds_02_ff4                 <= 32'b0;        
        dt_02_ff4                 <= 32'b0;  
        dprimary_r_02_ff4         <= 9'b0;        
        dprimary_g_02_ff4         <= 9'b0;        
        dprimary_b_02_ff4         <= 9'b0;        
        dprimary_a_02_ff4         <= 9'b0;        
        dz_12_ff4                 <= 48'b0;        
        dw_12_ff4                 <= 32'b0;        
        ds_12_ff4                 <= 32'b0;        
        dt_12_ff4                 <= 32'b0;        
        dprimary_r_12_ff4         <=  8'b0;        
        dprimary_g_12_ff4         <=  8'b0;        
        dprimary_b_12_ff4         <=  8'b0;        
        dprimary_a_12_ff4         <=  8'b0;        
        x_in_ff4                  <= 32'b0;
        y_in_ff4                  <= 32'b0; 
        vertex2_z_ff5             <= 48'b0;   
        vertex2_w_ff5             <= 32'b0;   
        vertex2_s_ff5             <= 32'b0;   
        vertex2_t_ff5             <= 32'b0;   
        vertex2_primary_r_ff5     <=  8'b0;   
        vertex2_primary_g_ff5     <=  8'b0;  
        vertex2_primary_b_ff5     <=  8'b0;  
        vertex2_primary_a_ff5     <=  8'b0;  
        dz_02_ff5                 <= 48'b0;        
        dw_02_ff5                 <= 32'b0;
        ds_02_ff5                 <= 32'b0;        
        dt_02_ff5                 <= 32'b0;  
        dprimary_r_02_ff5         <= 9'b0;        
        dprimary_g_02_ff5         <= 9'b0;        
        dprimary_b_02_ff5         <= 9'b0;        
        dprimary_a_02_ff5         <= 9'b0;        
        dz_12_ff5                 <= 48'b0;        
        dw_12_ff5                 <= 32'b0;        
        ds_12_ff5                 <= 32'b0;        
        dt_12_ff5                 <= 32'b0;        
        dprimary_r_12_ff5         <=  8'b0;        
        dprimary_g_12_ff5         <=  8'b0;        
        dprimary_b_12_ff5         <=  8'b0;        
        dprimary_a_12_ff5         <=  8'b0;        
        x_in_ff5                  <= 32'b0;
        y_in_ff5                  <= 32'b0; 
        vertex2_z_ff6             <= 48'b0;   
        vertex2_w_ff6             <= 32'b0;   
        vertex2_s_ff6             <= 32'b0;   
        vertex2_t_ff6             <= 32'b0;   
        vertex2_primary_r_ff6     <=  8'b0;   
        vertex2_primary_g_ff6     <=  8'b0;  
        vertex2_primary_b_ff6     <=  8'b0;  
        vertex2_primary_a_ff6     <=  8'b0;  
        dz_02_ff6                 <= 48'b0;        
        dw_02_ff6                 <= 32'b0;
        ds_02_ff6                 <= 32'b0;        
        dt_02_ff6                 <= 32'b0;  
        dprimary_r_02_ff6         <= 9'b0;        
        dprimary_g_02_ff6         <= 9'b0;        
        dprimary_b_02_ff6         <= 9'b0;        
        dprimary_a_02_ff6         <= 9'b0;        
        dz_12_ff6                 <= 48'b0;        
        dw_12_ff6                 <= 48'b0;        
        ds_12_ff6                 <= 32'b0;        
        dt_12_ff6                 <= 32'b0;        
        dprimary_r_12_ff6         <=  8'b0;        
        dprimary_g_12_ff6         <=  8'b0;        
        dprimary_b_12_ff6         <=  8'b0;        
        dprimary_a_12_ff6         <=  8'b0;        
        x_in_ff6                  <= 32'b0;
        y_in_ff6                  <= 32'b0; 
        vertex2_z_ff7             <= 48'b0;   
        vertex2_w_ff7             <= 32'b0;   
        vertex2_s_ff7             <= 32'b0;   
        vertex2_t_ff7             <= 32'b0;   
        vertex2_primary_r_ff7     <=  8'b0;   
        vertex2_primary_g_ff7     <=  8'b0;  
        vertex2_primary_b_ff7     <=  8'b0;  
        vertex2_primary_a_ff7     <=  8'b0;  
        dz_02_ff7                 <= 48'b0;        
        dw_02_ff7                 <= 32'b0;
        ds_02_ff7                 <= 32'b0;        
        dt_02_ff7                 <= 32'b0;  
        dprimary_r_02_ff7         <= 9'b0;        
        dprimary_g_02_ff7         <= 9'b0;        
        dprimary_b_02_ff7         <= 9'b0;        
        dprimary_a_02_ff7         <= 9'b0;        
        dz_12_ff7                 <= 48'b0;        
        dw_12_ff7                 <= 32'b0;        
        ds_12_ff7                 <= 32'b0;        
        dt_12_ff7                 <= 32'b0;        
        dprimary_r_12_ff7         <=  8'b0;        
        dprimary_g_12_ff7         <=  8'b0;        
        dprimary_b_12_ff7         <=  8'b0;        
        dprimary_a_12_ff7         <=  8'b0;        
        x_in_ff7                  <= 32'b0;
        y_in_ff7                  <= 32'b0; 
        vertex2_z_ff8             <= 48'b0;   
        vertex2_w_ff8             <= 32'b0;   
        vertex2_s_ff8             <= 32'b0;   
        vertex2_t_ff8             <= 32'b0;   
        vertex2_primary_r_ff8     <=  8'b0;   
        vertex2_primary_g_ff8     <=  8'b0;  
        vertex2_primary_b_ff8     <=  8'b0;  
        vertex2_primary_a_ff8     <=  8'b0;  
        dz_02_ff8                 <= 48'b0;        
        dw_02_ff8                 <= 48'b0;
        ds_02_ff8                 <= 32'b0;        
        dt_02_ff8                 <= 32'b0;  
        dprimary_r_02_ff8         <= 9'b0;        
        dprimary_g_02_ff8         <= 9'b0;        
        dprimary_b_02_ff8         <= 9'b0;        
        dprimary_a_02_ff8         <= 9'b0;        
        dz_12_ff8                 <= 48'b0;        
        dw_12_ff8                 <= 48'b0;        
        ds_12_ff8                 <= 32'b0;        
        dt_12_ff8                 <= 32'b0;        
        dprimary_r_12_ff8         <=  8'b0;        
        dprimary_g_12_ff8         <=  8'b0;        
        dprimary_b_12_ff8         <=  8'b0;        
        dprimary_a_12_ff8         <=  8'b0;        
        x_in_ff8                  <= 32'b0;
        y_in_ff8                  <= 32'b0;
        vertex2_z_ff9             <= 48'b0;   
        vertex2_w_ff9             <= 32'b0;   
        vertex2_s_ff9             <= 32'b0;   
        vertex2_t_ff9             <= 32'b0;   
        vertex2_primary_r_ff9     <=  8'b0;   
        vertex2_primary_g_ff9     <=  8'b0;  
        vertex2_primary_b_ff9     <=  8'b0;  
        vertex2_primary_a_ff9     <=  8'b0;  
        dz_02_ff9                 <= 48'b0;        
        dw_02_ff9                 <= 48'b0;
        ds_02_ff9                 <= 32'b0;        
        dt_02_ff9                 <= 32'b0;  
        dprimary_r_02_ff9         <= 9'b0;        
        dprimary_g_02_ff9         <= 9'b0;        
        dprimary_b_02_ff9         <= 9'b0;        
        dprimary_a_02_ff9         <= 9'b0;        
        dz_12_ff9                 <= 48'b0;        
        dw_12_ff9                 <= 48'b0;        
        ds_12_ff9                 <= 32'b0;        
        dt_12_ff9                 <= 32'b0;        
        dprimary_r_12_ff9         <=  8'b0;        
        dprimary_g_12_ff9         <=  8'b0;        
        dprimary_b_12_ff9         <=  8'b0;        
        dprimary_a_12_ff9         <=  8'b0;        
        x_in_ff9                  <= 32'b0;
        y_in_ff9                  <= 32'b0; 
    end
    else if(~busy)
    begin
        ee0_ff1                   <= ee0               ;
        ee1_ff1                   <= ee1               ;
        ee2_ff1                   <= ee2               ;
        ee0_ff2                   <= ee0_ff1           ;
        ee1_ff2                   <= ee1_ff1           ;
        ee2_ff2                   <= ee2_ff1           ;  
        area_ff1                  <= area              ;
        area_ff2                  <= area_ff1          ; 
        vertex2_z_ff1             <= vertex2_z         ;
        vertex2_w_ff1             <= vertex2_w         ;
        vertex2_s_ff1             <= vertex2_s        ;
        vertex2_t_ff1             <= vertex2_t        ;
        vertex2_primary_r_ff1     <= vertex2_primary_r ;
        vertex2_primary_g_ff1     <= vertex2_primary_g ;
        vertex2_primary_b_ff1     <= vertex2_primary_b ;
        vertex2_primary_a_ff1     <= vertex2_primary_a ;
        dz_02_ff1                 <= dz_02             ;     
        dw_02_ff1                 <= dw_02             ;
        ds_02_ff1                 <= ds_02            ;     
        dt_02_ff1                 <= dt_02            ;
        dprimary_r_02_ff1         <= dprimary_r_02     ;    
        dprimary_g_02_ff1         <= dprimary_g_02     ;    
        dprimary_b_02_ff1         <= dprimary_b_02     ;    
        dprimary_a_02_ff1         <= dprimary_a_02     ;    
        dz_12_ff1                 <= dz_12             ;     
        dw_12_ff1                 <= dw_12             ;     
        ds_12_ff1                 <= ds_12            ;     
        dt_12_ff1                 <= dt_12            ;     
        dprimary_r_12_ff1         <= dprimary_r_12     ;    
        dprimary_g_12_ff1         <= dprimary_g_12     ;    
        dprimary_b_12_ff1         <= dprimary_b_12     ;    
        dprimary_a_12_ff1         <= dprimary_a_12     ;    
        x_in_ff1                  <= x_in              ;
        y_in_ff1                  <= y_in              ;
        vertex2_z_ff2             <= vertex2_z_ff1;               
        vertex2_w_ff2             <= vertex2_w_ff1;               
        vertex2_s_ff2             <= vertex2_s_ff1;              
        vertex2_t_ff2             <= vertex2_t_ff1;              
        vertex2_primary_r_ff2     <= vertex2_primary_r_ff1;       
        vertex2_primary_g_ff2     <= vertex2_primary_g_ff1;      
        vertex2_primary_b_ff2     <= vertex2_primary_b_ff1;      
        vertex2_primary_a_ff2     <= vertex2_primary_a_ff1;      
        dz_02_ff2                 <= dz_02_ff1;                        
        dw_02_ff2                 <= dw_02_ff1;                
        ds_02_ff2                 <= ds_02_ff1;                       
        dt_02_ff2                 <= dt_02_ff1;                 
        dprimary_r_02_ff2         <= dprimary_r_02_ff1;               
        dprimary_g_02_ff2         <= dprimary_g_02_ff1;               
        dprimary_b_02_ff2         <= dprimary_b_02_ff1;               
        dprimary_a_02_ff2         <= dprimary_a_02_ff1;               
        dz_12_ff2                 <= dz_12_ff1;                        
        dw_12_ff2                 <= dw_12_ff1;                        
        ds_12_ff2                 <= ds_12_ff1;                       
        dt_12_ff2                 <= dt_12_ff1;                       
        dprimary_r_12_ff2         <= dprimary_r_12_ff1;                
        dprimary_g_12_ff2         <= dprimary_g_12_ff1;                
        dprimary_b_12_ff2         <= dprimary_b_12_ff1;                
        dprimary_a_12_ff2         <= dprimary_a_12_ff1;                
        x_in_ff2                  <= x_in_ff1;                 
        y_in_ff2                  <= y_in_ff1;                  
        vertex2_z_ff3             <= vertex2_z_ff2;               
        vertex2_w_ff3             <= vertex2_w_ff2;               
        vertex2_s_ff3             <= vertex2_s_ff2;              
        vertex2_t_ff3             <= vertex2_t_ff2;              
        vertex2_primary_r_ff3     <= vertex2_primary_r_ff2;       
        vertex2_primary_g_ff3     <= vertex2_primary_g_ff2;      
        vertex2_primary_b_ff3     <= vertex2_primary_b_ff2;      
        vertex2_primary_a_ff3     <= vertex2_primary_a_ff2;      
        dz_02_ff3                 <= dz_02_ff2;                        
        dw_02_ff3                 <= dw_02_ff2;                
        ds_02_ff3                 <= ds_02_ff2;                       
        dt_02_ff3                 <= dt_02_ff2;                 
        dprimary_r_02_ff3         <= dprimary_r_02_ff2;               
        dprimary_g_02_ff3         <= dprimary_g_02_ff2;               
        dprimary_b_02_ff3         <= dprimary_b_02_ff2;               
        dprimary_a_02_ff3         <= dprimary_a_02_ff2;               
        dz_12_ff3                 <= dz_12_ff2;                        
        dw_12_ff3                 <= dw_12_ff2;                        
        ds_12_ff3                 <= ds_12_ff2;                       
        dt_12_ff3                 <= dt_12_ff2;                       
        dprimary_r_12_ff3         <= dprimary_r_12_ff2;                
        dprimary_g_12_ff3         <= dprimary_g_12_ff2;                
        dprimary_b_12_ff3         <= dprimary_b_12_ff2;                
        dprimary_a_12_ff3         <= dprimary_a_12_ff2;                
        x_in_ff3                  <= x_in_ff2;                 
        y_in_ff3                  <= y_in_ff2;                  
        vertex2_z_ff4             <= vertex2_z_ff3;               
        vertex2_w_ff4             <= vertex2_w_ff3;               
        vertex2_s_ff4             <= vertex2_s_ff3;              
        vertex2_t_ff4             <= vertex2_t_ff3;              
        vertex2_primary_r_ff4     <= vertex2_primary_r_ff3;       
        vertex2_primary_g_ff4     <= vertex2_primary_g_ff3;      
        vertex2_primary_b_ff4     <= vertex2_primary_b_ff3;      
        vertex2_primary_a_ff4     <= vertex2_primary_a_ff3;      
        dz_02_ff4                 <= dz_02_ff3;                        
        dw_02_ff4                 <= dw_02_ff3;                
        ds_02_ff4                 <= ds_02_ff3;                       
        dt_02_ff4                 <= dt_02_ff3;                 
        dprimary_r_02_ff4         <= dprimary_r_02_ff3;               
        dprimary_g_02_ff4         <= dprimary_g_02_ff3;               
        dprimary_b_02_ff4         <= dprimary_b_02_ff3;               
        dprimary_a_02_ff4         <= dprimary_a_02_ff3;               
        dz_12_ff4                 <= dz_12_ff3;                        
        dw_12_ff4                 <= dw_12_ff3;                        
        ds_12_ff4                 <= ds_12_ff3;                       
        dt_12_ff4                 <= dt_12_ff3;                       
        dprimary_r_12_ff4         <= dprimary_r_12_ff3;                
        dprimary_g_12_ff4         <= dprimary_g_12_ff3;                
        dprimary_b_12_ff4         <= dprimary_b_12_ff3;                
        dprimary_a_12_ff4         <= dprimary_a_12_ff3;                
        x_in_ff4                  <= x_in_ff3;                 
        y_in_ff4                  <= y_in_ff3;                  
        vertex2_z_ff5             <= vertex2_z_ff4;               
        vertex2_w_ff5             <= vertex2_w_ff4;               
        vertex2_s_ff5             <= vertex2_s_ff4;              
        vertex2_t_ff5             <= vertex2_t_ff4;              
        vertex2_primary_r_ff5     <= vertex2_primary_r_ff4;       
        vertex2_primary_g_ff5     <= vertex2_primary_g_ff4;      
        vertex2_primary_b_ff5     <= vertex2_primary_b_ff4;      
        vertex2_primary_a_ff5     <= vertex2_primary_a_ff4;      
        dz_02_ff5                 <= dz_02_ff4;                        
        dw_02_ff5                 <= dw_02_ff4;                
        ds_02_ff5                 <= ds_02_ff4;                       
        dt_02_ff5                 <= dt_02_ff4;                 
        dprimary_r_02_ff5         <= dprimary_r_02_ff4;               
        dprimary_g_02_ff5         <= dprimary_g_02_ff4;               
        dprimary_b_02_ff5         <= dprimary_b_02_ff4;               
        dprimary_a_02_ff5         <= dprimary_a_02_ff4;               
        dz_12_ff5                 <= dz_12_ff4;                        
        dw_12_ff5                 <= dw_12_ff4;                        
        ds_12_ff5                 <= ds_12_ff4;                       
        dt_12_ff5                 <= dt_12_ff4;                       
        dprimary_r_12_ff5         <= dprimary_r_12_ff4;                
        dprimary_g_12_ff5         <= dprimary_g_12_ff4;                
        dprimary_b_12_ff5         <= dprimary_b_12_ff4;                
        dprimary_a_12_ff5         <= dprimary_a_12_ff4;                
        x_in_ff5                  <= x_in_ff4;                 
        y_in_ff5                  <= y_in_ff4;                  
        vertex2_z_ff6             <= vertex2_z_ff5;               
        vertex2_w_ff6             <= vertex2_w_ff5;               
        vertex2_s_ff6             <= vertex2_s_ff5;              
        vertex2_t_ff6             <= vertex2_t_ff5;              
        vertex2_primary_r_ff6     <= vertex2_primary_r_ff5;       
        vertex2_primary_g_ff6     <= vertex2_primary_g_ff5;      
        vertex2_primary_b_ff6     <= vertex2_primary_b_ff5;      
        vertex2_primary_a_ff6     <= vertex2_primary_a_ff5;      
        dz_02_ff6                 <= dz_02_ff5;                        
        dw_02_ff6                 <= dw_02_ff5;                
        ds_02_ff6                 <= ds_02_ff5;                       
        dt_02_ff6                 <= dt_02_ff5;                 
        dprimary_r_02_ff6         <= dprimary_r_02_ff5;               
        dprimary_g_02_ff6         <= dprimary_g_02_ff5;               
        dprimary_b_02_ff6         <= dprimary_b_02_ff5;               
        dprimary_a_02_ff6         <= dprimary_a_02_ff5;               
        dz_12_ff6                 <= dz_12_ff5;                        
        dw_12_ff6                 <= dw_12_ff5;                        
        ds_12_ff6                 <= ds_12_ff5;                       
        dt_12_ff6                 <= dt_12_ff5;                       
        dprimary_r_12_ff6         <= dprimary_r_12_ff5;                
        dprimary_g_12_ff6         <= dprimary_g_12_ff5;                
        dprimary_b_12_ff6         <= dprimary_b_12_ff5;                
        dprimary_a_12_ff6         <= dprimary_a_12_ff5;                
        x_in_ff6                  <= x_in_ff5;                 
        y_in_ff6                  <= y_in_ff5;                  
        vertex2_z_ff7             <= vertex2_z_ff6;               
        vertex2_w_ff7             <= vertex2_w_ff6;               
        vertex2_s_ff7             <= vertex2_s_ff6;              
        vertex2_t_ff7             <= vertex2_t_ff6;              
        vertex2_primary_r_ff7     <= vertex2_primary_r_ff6;       
        vertex2_primary_g_ff7     <= vertex2_primary_g_ff6;      
        vertex2_primary_b_ff7     <= vertex2_primary_b_ff6;      
        vertex2_primary_a_ff7     <= vertex2_primary_a_ff6;      
        dz_02_ff7                 <= dz_02_ff6;                        
        dw_02_ff7                 <= dw_02_ff6;                
        ds_02_ff7                 <= ds_02_ff6;                       
        dt_02_ff7                 <= dt_02_ff6;                 
        dprimary_r_02_ff7         <= dprimary_r_02_ff6;               
        dprimary_g_02_ff7         <= dprimary_g_02_ff6;               
        dprimary_b_02_ff7         <= dprimary_b_02_ff6;               
        dprimary_a_02_ff7         <= dprimary_a_02_ff6;               
        dz_12_ff7                 <= dz_12_ff6;                        
        dw_12_ff7                 <= dw_12_ff6;                        
        ds_12_ff7                 <= ds_12_ff6;                       
        dt_12_ff7                 <= dt_12_ff6;                       
        dprimary_r_12_ff7         <= dprimary_r_12_ff6;                
        dprimary_g_12_ff7         <= dprimary_g_12_ff6;                
        dprimary_b_12_ff7         <= dprimary_b_12_ff6;                
        dprimary_a_12_ff7         <= dprimary_a_12_ff6;                
        x_in_ff7                  <= x_in_ff6;                 
        y_in_ff7                  <= y_in_ff6;                  
        vertex2_z_ff8             <= vertex2_z_ff7;               
        vertex2_w_ff8             <= vertex2_w_ff7;               
        vertex2_s_ff8             <= vertex2_s_ff7;              
        vertex2_t_ff8             <= vertex2_t_ff7;              
        vertex2_primary_r_ff8     <= vertex2_primary_r_ff7;       
        vertex2_primary_g_ff8     <= vertex2_primary_g_ff7;      
        vertex2_primary_b_ff8     <= vertex2_primary_b_ff7;      
        vertex2_primary_a_ff8     <= vertex2_primary_a_ff7;      
        dz_02_ff8                 <= dz_02_ff7;                        
        dw_02_ff8                 <= dw_02_ff7;                
        ds_02_ff8                 <= ds_02_ff7;                       
        dt_02_ff8                 <= dt_02_ff7;                 
        dprimary_r_02_ff8         <= dprimary_r_02_ff7;               
        dprimary_g_02_ff8         <= dprimary_g_02_ff7;               
        dprimary_b_02_ff8         <= dprimary_b_02_ff7;               
        dprimary_a_02_ff8         <= dprimary_a_02_ff7;               
        dz_12_ff8                 <= dz_12_ff7;                        
        dw_12_ff8                 <= dw_12_ff7;                        
        ds_12_ff8                 <= ds_12_ff7;                       
        dt_12_ff8                 <= dt_12_ff7;                       
        dprimary_r_12_ff8         <= dprimary_r_12_ff7;                
        dprimary_g_12_ff8         <= dprimary_g_12_ff7;                
        dprimary_b_12_ff8         <= dprimary_b_12_ff7;                
        dprimary_a_12_ff8         <= dprimary_a_12_ff7;                
        x_in_ff8                  <= x_in_ff7;                 
        y_in_ff8                  <= y_in_ff7;
        vertex2_z_ff9             <= vertex2_z_ff8;               
        vertex2_w_ff9             <= vertex2_w_ff8;               
        vertex2_s_ff9             <= vertex2_s_ff8;              
        vertex2_t_ff9             <= vertex2_t_ff8;              
        vertex2_primary_r_ff9     <= vertex2_primary_r_ff8;       
        vertex2_primary_g_ff9     <= vertex2_primary_g_ff8;      
        vertex2_primary_b_ff9     <= vertex2_primary_b_ff8;      
        vertex2_primary_a_ff9     <= vertex2_primary_a_ff8;      
        dz_02_ff9                 <= dz_02_ff8;                        
        dw_02_ff9                 <= dw_02_ff8;                
        ds_02_ff9                 <= ds_02_ff8;                       
        dt_02_ff9                 <= dt_02_ff8;                 
        dprimary_r_02_ff9         <= dprimary_r_02_ff8;               
        dprimary_g_02_ff9         <= dprimary_g_02_ff8;               
        dprimary_b_02_ff9         <= dprimary_b_02_ff8;               
        dprimary_a_02_ff9         <= dprimary_a_02_ff8;               
        dz_12_ff9                 <= dz_12_ff8;                        
        dw_12_ff9                 <= dw_12_ff8;                        
        ds_12_ff9                 <= ds_12_ff8;                       
        dt_12_ff9                 <= dt_12_ff8;                       
        dprimary_r_12_ff9         <= dprimary_r_12_ff8;                
        dprimary_g_12_ff9         <= dprimary_g_12_ff8;                
        dprimary_b_12_ff9         <= dprimary_b_12_ff8;                
        dprimary_a_12_ff9         <= dprimary_a_12_ff8;                
        x_in_ff9                  <= x_in_ff8;                 
        y_in_ff9                  <= y_in_ff8; 

    end
end

//******************************
//stage 1 cal k1, k2
//******************************
//convert ee1,ee2,area to float
//
//cycle 1-2 count leading zero
wire [95:0] ee1_unsigned = ee1[95] ? -ee1 : ee1;
wire [95:0] ee2_unsigned = ee2[95] ? -ee2 : ee2;  
wire [95:0] area_unsigned = area[95] ? -area : area; 


wire [6:0] ee1_lz;
wire [6:0] ee2_lz;
wire [6:0] area_lz;
wire lz_valid; 

clz_96 clz_ee1(
    .clk(clk),
    .rst_n(rst_n),
    .busy(busy),
    .Data_en(xy_in_en),
    .Data_in(ee1_unsigned),
    .LZB_out(ee1_lz),
    .LZB_valid(lz_valid)
); 

clz_96 clz_ee2(
    .clk(clk),
    .rst_n(rst_n),
    .busy(busy),
    .Data_en(xy_in_en),
    .Data_in(ee2_unsigned),
    .LZB_out(ee2_lz)
);  

clz_96 clz_area(
    .clk(clk),
    .rst_n(rst_n),
    .busy(busy),
    .Data_en(xy_in_en),
    .Data_in(area_unsigned),
    .LZB_out(area_lz)
);

reg [95:0] ee1_unsigned_reg ;
reg [95:0] ee2_unsigned_reg ;
reg [95:0] area_unsigned_reg ;  

reg [95:0] pre_ee1_unsigned_reg ;
reg [95:0] pre_ee2_unsigned_reg ;
reg [95:0] pre_area_unsigned_reg ;  

always@(posedge clk or negedge rst_n)
begin
   if(~rst_n)
   begin
        pre_ee1_unsigned_reg  <= 96'b0 ;
        pre_ee2_unsigned_reg  <= 96'b0 ;
        pre_area_unsigned_reg <= 96'b0 ; 
   end
   else if(~busy && xy_in_en)
   begin
        pre_ee1_unsigned_reg  <= ee1_unsigned;
        pre_ee2_unsigned_reg  <= ee2_unsigned;
        pre_area_unsigned_reg <= area_unsigned;    
   end
end

always@(posedge clk or negedge rst_n)
begin
   if(~rst_n)
   begin
        ee1_unsigned_reg  <= 96'b0 ;
        ee2_unsigned_reg  <= 96'b0 ;
        area_unsigned_reg <= 96'b0 ; 
   end
   else if(~busy)
   begin
        ee1_unsigned_reg  <= pre_ee1_unsigned_reg ;
        ee2_unsigned_reg  <= pre_ee2_unsigned_reg ;
        area_unsigned_reg <= pre_area_unsigned_reg;    
   end
end    

//cycle 3 calculate exp & man to generate float
//
wire [7:0] ee1_exp_unscale = 127 + 95 - ee1_lz ;
wire [7:0] ee2_exp_unscale = 127 + 95 - ee2_lz ; 
wire [7:0] ee1_exp = 127 + 95 - ee1_lz + 32;
wire [7:0] ee2_exp = 127 + 95 - ee2_lz + 32;
wire [7:0] area_exp = 127 + 95 - area_lz;
wire [22:0] ee1_man =( ee1_unsigned_reg << (ee1_lz+1)) >> 73; //man of ee`_unsigned, ee1_unsigned(64.32), first non_zero bit 96 - s_lz, left shift s_lz + 1 moved the bit after fisrt non-zero bit to MSB, then rigth shift 73 to fit the 23 bit size for IEEE 754 float point 
wire [22:0] ee2_man =( ee2_unsigned_reg << (ee2_lz+1)) >> 73; //man of t_unsgnied 
wire [22:0] area_man =( area_unsigned_reg << (area_lz+1)) >> 73; //man of w_unsigned

reg [31:0] ee1_float;
reg [31:0] ee2_float;
reg [31:0] area_float;
reg div_en;

always@(posedge clk or negedge rst_n)
begin
   if(~rst_n)
   begin
        ee1_float  <= 32'b0;
        ee2_float  <= 32'b0;
        area_float <= 32'b0;
   end
   else if(~busy && lz_valid)
   begin
        ee1_float  <= {ee1_ff2[95],ee1_exp,ee1_man};
        ee2_float  <= {ee2_ff2[95],ee2_exp,ee2_man};
        area_float <= {area_ff2[95],area_exp,area_man}; 
   end
end


reg [31:0] ee1_float_unscale;
reg [31:0] ee2_float_unscale; 
always@(posedge clk or negedge rst_n)
begin
   if(~rst_n)
   begin
        ee1_float_unscale  <= 32'b0;
        ee2_float_unscale  <= 32'b0;
   end
   else if(~busy && lz_valid)
   begin
        ee1_float_unscale  <= {ee1_ff2[95],ee1_exp_unscale,ee1_man};
        ee2_float_unscale  <= {ee2_ff2[95],ee2_exp_unscale,ee2_man};
   end
end   

always@(posedge clk or negedge rst_n)
begin
    if(~rst_n)
    begin
        div_en  <= 1'b0;
    end
    else if(~busy)
    begin
        if(lz_valid)
            div_en <= 1'b1;
        else
            div_en <= 1'b0;
   end
end

//float division, cycle 4-9
wire [31:0] k1;
wire [31:0] k2;
wire [31:0] k1_float;
wire [31:0] k2_float; 
wire div_valid;
gc_ifdiv u_ee1_div_area(
    .clk(clk),                     //input clock
    .rst_n(rst_n),                 //input reset, low active
    .busy(busy),                   //input busy
    .en(div_en),                   //input enable
    .type_a(2'b10),                //input a type,2'b10:float 2'b01:signed 2'b00:unsigned 
    .type_b(2'b10),                //input b type,2'b10:float 2'b01:signed 2'b00:unsigned 
    .a(ee1_float),                 //input a
    .b(area_float),                //input b
    .type_q(2'b0),                 //output b type,2'b10:float 2'b01:signed 2'b00:unsigned
    .valid(div_valid),             //output valid
    .quotient(k1)                  //output data
);  
    
gc_ifdiv u_ee2_div_area(
    .clk(clk),                     //input clock
    .rst_n(rst_n),                 //input reset, low active
    .busy(busy),                   //input busy
    .en(div_en),                   //input enable
    .type_a(2'b10),                //input a type,2'b10:float 2'b01:signed 2'b00:unsigned 
    .type_b(2'b10),                //input b type,2'b10:float 2'b01:signed 2'b00:unsigned 
    .a(ee2_float),                 //input a
    .b(area_float),                //input b
    .type_q(2'b0),                 //output b type,2'b10:float 2'b01:signed 2'b00:unsigned
    .valid(),             //output valid
    .quotient(k2)                  //output data
); 

gc_ifdiv u_ee1_div_area_float(
    .clk(clk),                     //input clock
    .rst_n(rst_n),                 //input reset, low active
    .busy(busy),                   //input busy
    .en(div_en),                   //input enable
    .type_a(2'b10),                //input a type,2'b10:float 2'b01:signed 2'b00:unsigned 
    .type_b(2'b10),                //input b type,2'b10:float 2'b01:signed 2'b00:unsigned 
    .a(ee1_float_unscale),                 //input a
    .b(area_float),                //input b
    .type_q(2'b10),                 //output b type,2'b10:float 2'b01:signed 2'b00:unsigned
    .valid(),             //output valid
    .quotient(k1_float)                  //output data
);  
    
gc_ifdiv u_ee2_div_area_float(
    .clk(clk),                     //input clock
    .rst_n(rst_n),                 //input reset, low active
    .busy(busy),                   //input busy
    .en(div_en),                   //input enable
    .type_a(2'b10),                //input a type,2'b10:float 2'b01:signed 2'b00:unsigned 
    .type_b(2'b10),                //input b type,2'b10:float 2'b01:signed 2'b00:unsigned 
    .a(ee2_float_unscale),                 //input a
    .b(area_float),                //input b
    .type_q(2'b10),                 //output b type,2'b10:float 2'b01:signed 2'b00:unsigned
    .valid(),             //output valid
    .quotient(k2_float)                  //output data
);   
    
//*************************************************************
//stage 2, interpolator, 1 cycle for rgba & z, 8 cycles for stw
//*************************************************************
wire [7:0] r_int; 
wire [7:0] g_int;
wire [7:0] b_int;
wire [7:0] a_int;
wire int_valid;
interpolator_rgb u_r_int(
    .clk(clk),
    .rst_n(rst_n),
    .int_en(div_valid),
    .busy(busy),
    .a0(vertex2_primary_r_ff9), 
    .d_a1(dprimary_r_02_ff9), 
    .d_a2(dprimary_r_12_ff9), 
    .k1(k1), 
    .k2(k2), 
    .a_out(r_int), 
    .out_valid()
); 

interpolator_rgb u_g_int(
    .clk(clk),
    .rst_n(rst_n),
    .int_en(div_valid),
    .busy(busy),
    .a0(vertex2_primary_g_ff9), 
    .d_a1(dprimary_g_02_ff9), 
    .d_a2(dprimary_g_12_ff9), 
    .k1(k1), 
    .k2(k2), 
    .a_out(g_int), 
    .out_valid()
);  

interpolator_rgb u_b_int(
    .clk(clk),
    .rst_n(rst_n),
    .int_en(div_valid),
    .busy(busy),
    .a0(vertex2_primary_b_ff9), 
    .d_a1(dprimary_b_02_ff9), 
    .d_a2(dprimary_b_12_ff9), 
    .k1(k1), 
    .k2(k2), 
    .a_out(b_int), 
    .out_valid()
);  

interpolator_rgb u_a_int(
    .clk(clk),
    .rst_n(rst_n),
    .int_en(div_valid),
    .busy(busy),
    .a0(vertex2_primary_a_ff9), 
    .d_a1(dprimary_a_02_ff9), 
    .d_a2(dprimary_a_12_ff9), 
    .k1(k1), 
    .k2(k2), 
    .a_out(a_int), 
    .out_valid()
); 


wire [31:0] z_int;


//float int, 8 cycle
wire [31:0] s_int;
wire [31:0] t_int;
wire [31:0] w_int;

interpolator u_z_int(
    .clk(clk),
    .rst_n(rst_n),
    .int_en(div_valid),
    .busy(busy),
    .a0(vertex2_z_ff9), 
    .d_a1(dz_02_ff9), 
    .d_a2(dz_12_ff9), 
    .k1(k1), 
    .k2(k2), 
    .a_out(z_int), 
    .out_valid()
);

interpolator_float u_s_int(
    .clk(clk),
    .rst_n(rst_n),
    .int_en(div_valid),
    .busy(busy),
    .a0(vertex2_s_ff9), 
    .d_a1(ds_02_ff9), 
    .d_a2(ds_12_ff9), 
    .k1(k1_float), 
    .k2(k2_float), 
    .a_out(s_int), 
    .out_valid(int_valid)
);

interpolator_float u_t_int(
    .clk(clk),
    .rst_n(rst_n),
    .int_en(div_valid),
    .busy(busy),
    .a0(vertex2_t_ff9), 
    .d_a1(dt_02_ff9), 
    .d_a2(dt_12_ff9), 
    .k1(k1_float), 
    .k2(k2_float), 
    .a_out(t_int), 
    .out_valid()
); 

interpolator_float u_w_int(
    .clk(clk),
    .rst_n(rst_n),
    .int_en(div_valid),
    .busy(busy),
    .a0(vertex2_w_ff9), 
    .d_a1(dw_02_ff9), 
    .d_a2(dw_12_ff9), 
    .k1(k1_float), 
    .k2(k2_float), 
    .a_out(w_int), 
    .out_valid()
);   

reg [31:0] x_in_ff10;
reg [31:0] y_in_ff10;
reg [31:0] x_in_ff11;
reg [31:0] y_in_ff11;
reg [31:0] x_in_ff12;
reg [31:0] y_in_ff12;
reg [31:0] x_in_ff13;
reg [31:0] y_in_ff13;
reg [31:0] x_in_ff14;
reg [31:0] y_in_ff14;
reg [31:0] x_in_ff15;
reg [31:0] y_in_ff15;
reg [31:0] x_in_ff16;
reg [31:0] y_in_ff16;   
reg [31:0] x_in_ff17;
reg [31:0] y_in_ff17;   


always@(posedge clk or negedge rst_n)
begin
    if(~rst_n)
    begin
        x_in_ff10 <= 32'b0;
        y_in_ff10 <= 32'b0;
        x_in_ff11 <= 32'b0;
        y_in_ff11 <= 32'b0;
        x_in_ff12 <= 32'b0;
        y_in_ff12 <= 32'b0;
        x_in_ff13 <= 32'b0;
        y_in_ff13 <= 32'b0;
        x_in_ff14 <= 32'b0;
        y_in_ff14 <= 32'b0;
        x_in_ff15 <= 32'b0;
        y_in_ff15 <= 32'b0;
        x_in_ff16 <= 32'b0;
        y_in_ff16 <= 32'b0;
        x_in_ff17 <= 32'b0;
        y_in_ff17 <= 32'b0;
    end
    else if(~busy)
    begin
        x_in_ff10 <= x_in_ff9;
        y_in_ff10 <= y_in_ff9;
        x_in_ff11 <= x_in_ff10;
        y_in_ff11 <= y_in_ff10;
        x_in_ff12 <= x_in_ff11;
        y_in_ff12 <= y_in_ff11;
        x_in_ff13 <= x_in_ff12;
        y_in_ff13 <= y_in_ff12;
        x_in_ff14 <= x_in_ff13;
        y_in_ff14 <= y_in_ff13;
        x_in_ff15 <= x_in_ff14;
        y_in_ff15 <= y_in_ff14;
        x_in_ff16 <= x_in_ff15;
        y_in_ff16 <= y_in_ff15;
        x_in_ff17 <= x_in_ff16;
        y_in_ff17 <= y_in_ff16; 
    end
end

reg [7:0] r_int_ff1; 
reg [7:0] g_int_ff1;
reg [7:0] b_int_ff1;
reg [7:0] a_int_ff1; 
reg [7:0] r_int_ff2; 
reg [7:0] g_int_ff2;
reg [7:0] b_int_ff2;
reg [7:0] a_int_ff2; 
reg [7:0] r_int_ff3; 
reg [7:0] g_int_ff3;
reg [7:0] b_int_ff3;
reg [7:0] a_int_ff3; 
reg [7:0] r_int_ff4; 
reg [7:0] g_int_ff4;
reg [7:0] b_int_ff4;
reg [7:0] a_int_ff4; 
reg [7:0] r_int_ff5; 
reg [7:0] g_int_ff5;
reg [7:0] b_int_ff5;
reg [7:0] a_int_ff5;
reg [7:0] r_int_ff6; 
reg [7:0] g_int_ff6;
reg [7:0] b_int_ff6;
reg [7:0] a_int_ff6; 
reg [7:0] r_int_ff7; 
reg [7:0] g_int_ff7;
reg [7:0] b_int_ff7;
reg [7:0] a_int_ff7; 
reg [31:0] z_int_ff1;
reg [31:0] z_int_ff2;
reg [31:0] z_int_ff3;
reg [31:0] z_int_ff4;
reg [31:0] z_int_ff5;
reg [31:0] z_int_ff6;
reg [31:0] z_int_ff7; 


always@(posedge clk or negedge rst_n)
begin
    if(~rst_n)
    begin
        r_int_ff1 <= 8'b0; 
        g_int_ff1 <= 8'b0;
        b_int_ff1 <= 8'b0;
        a_int_ff1 <= 8'b0; 
        r_int_ff2 <= 8'b0; 
        g_int_ff2 <= 8'b0;
        b_int_ff2 <= 8'b0;
        a_int_ff2 <= 8'b0; 
        r_int_ff3 <= 8'b0; 
        g_int_ff3 <= 8'b0;
        b_int_ff3 <= 8'b0;
        a_int_ff3 <= 8'b0; 
        r_int_ff4 <= 8'b0; 
        g_int_ff4 <= 8'b0;
        b_int_ff4 <= 8'b0;
        a_int_ff4 <= 8'b0; 
        r_int_ff5 <= 8'b0; 
        g_int_ff5 <= 8'b0;
        b_int_ff5 <= 8'b0;
        a_int_ff5 <= 8'b0;
        r_int_ff6 <= 8'b0; 
        g_int_ff6 <= 8'b0;
        b_int_ff6 <= 8'b0;
        a_int_ff6 <= 8'b0; 
        r_int_ff7 <= 8'b0; 
        g_int_ff7 <= 8'b0;
        b_int_ff7 <= 8'b0;
        a_int_ff7 <= 8'b0; 
        z_int_ff1 <= 32'b0;
        z_int_ff2 <= 32'b0;
        z_int_ff3 <= 32'b0;
        z_int_ff4 <= 32'b0;
        z_int_ff5 <= 32'b0;
        z_int_ff6 <= 32'b0;
        z_int_ff7 <= 32'b0;
    end
    else if(~busy)
    begin
        r_int_ff1 <= r_int; 
        g_int_ff1 <= g_int;
        b_int_ff1 <= b_int;
        a_int_ff1 <= a_int; 
        r_int_ff2 <= r_int_ff1; 
        g_int_ff2 <= g_int_ff1;
        b_int_ff2 <= b_int_ff1;
        a_int_ff2 <= a_int_ff1; 
        r_int_ff3 <= r_int_ff2; 
        g_int_ff3 <= g_int_ff2;
        b_int_ff3 <= b_int_ff2;
        a_int_ff3 <= a_int_ff2; 
        r_int_ff4 <= r_int_ff3; 
        g_int_ff4 <= g_int_ff3;
        b_int_ff4 <= b_int_ff3;
        a_int_ff4 <= a_int_ff3; 
        r_int_ff5 <= r_int_ff4; 
        g_int_ff5 <= g_int_ff4;
        b_int_ff5 <= b_int_ff4;
        a_int_ff5 <= a_int_ff4;
        r_int_ff6 <= r_int_ff5; 
        g_int_ff6 <= g_int_ff5;
        b_int_ff6 <= b_int_ff5;
        a_int_ff6 <= a_int_ff5; 
        r_int_ff7 <= r_int_ff6; 
        g_int_ff7 <= g_int_ff6;
        b_int_ff7 <= b_int_ff6;
        a_int_ff7 <= a_int_ff6;
        z_int_ff1 <= z_int;
        z_int_ff2 <= z_int_ff1;
        z_int_ff3 <= z_int_ff2;
        z_int_ff4 <= z_int_ff3;
        z_int_ff5 <= z_int_ff4;
        z_int_ff6 <= z_int_ff5;
        z_int_ff7 <= z_int_ff6;
    end
end  

//********************************
//stage 3, perspective correction,6 cycle
//********************************
reg [31:0] x_in_ff18;
reg [31:0] y_in_ff18;
reg [31:0] x_in_ff19;
reg [31:0] y_in_ff19;
reg [31:0] x_in_ff20;
reg [31:0] y_in_ff20;
reg [31:0] x_in_ff21;
reg [31:0] y_in_ff21;  
reg [31:0] x_in_ff22;
reg [31:0] y_in_ff22;
reg [31:0] x_in_ff23;
reg [31:0] y_in_ff23;

always@(posedge clk or negedge rst_n)
begin
    if(~rst_n)
    begin
        x_in_ff18 <= 32'b0;
        y_in_ff18 <= 32'b0;
        x_in_ff19 <= 32'b0;
        y_in_ff19 <= 32'b0;
        x_in_ff20 <= 32'b0;
        y_in_ff20 <= 32'b0; 
        x_in_ff21 <= 32'b0;
        y_in_ff21 <= 32'b0;
        x_in_ff22 <= 32'b0;
        y_in_ff22 <= 32'b0; 
        x_in_ff23 <= 32'b0;
        y_in_ff23 <= 32'b0; 
    end
    else if(~busy)
    begin
        x_in_ff18 <= x_in_ff17;
        y_in_ff18 <= y_in_ff17;
        x_in_ff19 <= x_in_ff18;
        y_in_ff19 <= y_in_ff18;
        x_in_ff20 <= x_in_ff19;
        y_in_ff20 <= y_in_ff19;
        x_in_ff21 <= x_in_ff20;
        y_in_ff21 <= y_in_ff20;
        x_in_ff22 <= x_in_ff21;
        y_in_ff22 <= y_in_ff21;
        x_in_ff23 <= x_in_ff22;
        y_in_ff23 <= y_in_ff22;
    end
end  


reg [7:0] r_int_ff8; 
reg [7:0] g_int_ff8;
reg [7:0] b_int_ff8;
reg [7:0] a_int_ff8; 
reg [7:0] r_int_ff9; 
reg [7:0] g_int_ff9;
reg [7:0] b_int_ff9;
reg [7:0] a_int_ff9; 
reg [7:0] r_int_ff10; 
reg [7:0] g_int_ff10;
reg [7:0] b_int_ff10;
reg [7:0] a_int_ff10;
reg [7:0] r_int_ff11; 
reg [7:0] g_int_ff11;
reg [7:0] b_int_ff11;
reg [7:0] a_int_ff11;
reg [7:0] r_int_ff12; 
reg [7:0] g_int_ff12;
reg [7:0] b_int_ff12;
reg [7:0] a_int_ff12;
reg [7:0] r_int_ff13; 
reg [7:0] g_int_ff13;
reg [7:0] b_int_ff13;
reg [7:0] a_int_ff13;  
reg [31:0] z_int_ff8;
reg [31:0] z_int_ff9;
reg [31:0] z_int_ff10;
reg [31:0] z_int_ff11;  
reg [31:0] z_int_ff12;
reg [31:0] z_int_ff13;   
reg [31:0] factor_m;

always@(posedge clk or negedge rst_n)
begin
    if(~rst_n)
        factor_m <= 32'b0;
    else
        factor_m <= factor * z_slope;
end  

always@(posedge clk or negedge rst_n)
begin
    if(~rst_n)
    begin
        r_int_ff8  <= 8'b0; 
        g_int_ff8  <= 8'b0;
        b_int_ff8  <= 8'b0;
        a_int_ff8  <= 8'b0; 
        r_int_ff9  <= 8'b0; 
        g_int_ff9  <= 8'b0;
        b_int_ff9  <= 8'b0;
        a_int_ff9  <= 8'b0; 
        r_int_ff10 <= 8'b0; 
        g_int_ff10 <= 8'b0;
        b_int_ff10 <= 8'b0;
        a_int_ff10 <= 8'b0;
        r_int_ff11 <= 8'b0; 
        g_int_ff11 <= 8'b0;
        b_int_ff11 <= 8'b0;
        a_int_ff11 <= 8'b0;
        r_int_ff12 <= 8'b0; 
        g_int_ff12 <= 8'b0;
        b_int_ff12 <= 8'b0;
        a_int_ff12 <= 8'b0;
        r_int_ff13 <= 8'b0; 
        g_int_ff13 <= 8'b0;
        b_int_ff13 <= 8'b0;
        a_int_ff13 <= 8'b0;
        z_int_ff8  <= 32'b0;
        z_int_ff9  <= 32'b0;
        z_int_ff10 <= 32'b0;
        z_int_ff11 <= 32'b0;
        z_int_ff12 <= 32'b0;
        z_int_ff13 <= 32'b0; 
    end
    else if(~busy)
    begin
        r_int_ff8  <= r_int_ff7 ; 
        g_int_ff8  <= g_int_ff7 ;
        b_int_ff8  <= b_int_ff7 ;
        a_int_ff8  <= a_int_ff7 ; 
        r_int_ff9  <= r_int_ff8 ; 
        g_int_ff9  <= g_int_ff8 ;
        b_int_ff9  <= b_int_ff8 ;
        a_int_ff9  <= a_int_ff8 ; 
        r_int_ff10 <= r_int_ff9 ; 
        g_int_ff10 <= g_int_ff9 ;
        b_int_ff10 <= b_int_ff9 ;
        a_int_ff10 <= a_int_ff9 ;
        r_int_ff11 <= r_int_ff10; 
        g_int_ff11 <= g_int_ff10;
        b_int_ff11 <= b_int_ff10;
        a_int_ff11 <= a_int_ff10;
        r_int_ff12 <= r_int_ff11; 
        g_int_ff12 <= g_int_ff11;
        b_int_ff12 <= b_int_ff11;
        a_int_ff12 <= a_int_ff11;
        r_int_ff13 <= r_int_ff12; 
        g_int_ff13 <= g_int_ff12;
        b_int_ff13 <= b_int_ff12;
        a_int_ff13 <= a_int_ff12;
        z_int_ff8  <= z_int_ff7 ;
        z_int_ff9  <= z_int_ff8 ;
        z_int_ff10 <= z_int_ff9 ;
        z_int_ff11 <= z_int_ff10;
        if(depth_offset_enable)
        begin
            z_int_ff12 <= z_int_ff11 + unit;
            z_int_ff13 <= z_int_ff12 + factor_m; 
        end
        else
        begin
            z_int_ff12 <= z_int_ff11;
            z_int_ff13 <= z_int_ff12; 
        end 
    end
end  
wire xy_out_en_wire;
wire [7:0] s_int_e_scaled = s_int[30:23] + 8'd16;
wire [7:0] t_int_e_scaled = t_int[30:23] + 8'd16; 
wire [31:0] s_int_scaled = {s_int[31],s_int_e_scaled,s_int[22:0]};
wire [31:0] t_int_scaled = {t_int[31],t_int_e_scaled,t_int[22:0]};
gc_ifdiv u_s_div_w(
    .clk(clk),                     //input clock
    .rst_n(rst_n),                 //input reset, low active
    .busy(busy),                   //input busy
    .en(int_valid),                  //input enable
    .type_a(2'b10),                //input a type,2'b10:float 2'b01:signed 2'b00:unsigned 
    .type_b(2'b10),                //input b type,2'b10:float 2'b01:signed 2'b00:unsigned 
    .a(s_int_scaled),       //input a
    .b(w_int),                  //input b
    .type_q(2'b1),                 //output b type,2'b10:float 2'b01:signed 2'b00:unsigned
    .valid(xy_out_en_wire),             //output valid
    .quotient(s)             //output data
);  

gc_ifdiv u_t_div_w(
    .clk(clk),                     //input clock
    .rst_n(rst_n),                 //input reset, low active
    .busy(busy),                   //input busy
    .en(int_valid),                  //input enable
    .type_a(2'b10),                //input a type,2'b10:float 2'b01:signed 2'b00:unsigned 
    .type_b(2'b10),                //input b type,2'b10:float 2'b01:signed 2'b00:unsigned 
    .a(t_int_scaled),       //input a
    .b(w_int),                  //input b
    .type_q(2'b1),                 //output b type,2'b10:float 2'b01:signed 2'b00:unsigned
    .valid(),             //output valid
    .quotient(t)             //output data
); 
assign x_out      = x_in_ff23;
assign y_out      = y_in_ff23;
assign z_out      = z_int_ff13;
assign primary_r  = r_int_ff13;              
assign primary_g  = g_int_ff13;              
assign primary_b  = b_int_ff13;              
assign primary_a  = a_int_ff13;              

reg mode_end_ff1;
reg mode_end_ff2;
reg mode_end_ff3;
reg mode_end_ff4;
reg mode_end_ff5;
reg mode_end_ff6;
reg mode_end_ff7;
reg mode_end_ff8;
reg mode_end_ff9;
reg mode_end_ff10;
reg mode_end_ff11;
reg mode_end_ff12;
reg mode_end_ff13;
reg mode_end_ff14;
reg mode_end_ff15;
reg mode_end_ff16;
reg mode_end_ff17;
reg mode_end_ff18;
reg mode_end_ff19;
reg mode_end_ff20;
reg mode_end_ff21;
reg mode_end_ff22;
reg mode_end_ff23; 

always@(posedge clk or negedge rst_n)
begin
    if(~rst_n)
    begin
        mode_end_ff1  <= 1'b0;
        mode_end_ff2  <= 1'b0;
        mode_end_ff3  <= 1'b0;
        mode_end_ff4  <= 1'b0;
        mode_end_ff5  <= 1'b0;
        mode_end_ff6  <= 1'b0;
        mode_end_ff7  <= 1'b0;
        mode_end_ff8  <= 1'b0;
        mode_end_ff9  <= 1'b0;
        mode_end_ff10 <= 1'b0;
        mode_end_ff11 <= 1'b0;
        mode_end_ff12 <= 1'b0;
        mode_end_ff13 <= 1'b0;
        mode_end_ff14 <= 1'b0;
        mode_end_ff15 <= 1'b0;
        mode_end_ff16 <= 1'b0; 
        mode_end_ff17 <= 1'b0;
        mode_end_ff18 <= 1'b0;
        mode_end_ff19 <= 1'b0;
        mode_end_ff20 <= 1'b0;
        mode_end_ff21 <= 1'b0;
        mode_end_ff22 <= 1'b0;
        mode_end_ff23 <= 1'b0; 
    end
    else if(~busy)
    begin
        mode_end_ff1  <= mode_end;
        mode_end_ff2  <= mode_end_ff1 ;
        mode_end_ff3  <= mode_end_ff2 ;
        mode_end_ff4  <= mode_end_ff3 ;
        mode_end_ff5  <= mode_end_ff4 ;
        mode_end_ff6  <= mode_end_ff5 ;
        mode_end_ff7  <= mode_end_ff6 ;
        mode_end_ff8  <= mode_end_ff7 ;
        mode_end_ff9  <= mode_end_ff8 ;
        mode_end_ff10 <= mode_end_ff9 ;
        mode_end_ff11 <= mode_end_ff10;
        mode_end_ff12 <= mode_end_ff11;
        mode_end_ff13 <= mode_end_ff12;
        mode_end_ff14 <= mode_end_ff13;
        mode_end_ff15 <= mode_end_ff14;
        mode_end_ff16 <= mode_end_ff15;
        mode_end_ff17 <= mode_end_ff16;
        mode_end_ff18 <= mode_end_ff17;
        mode_end_ff19 <= mode_end_ff18;
        mode_end_ff20 <= mode_end_ff19;
        mode_end_ff21 <= mode_end_ff20;
        mode_end_ff22 <= mode_end_ff21;
        mode_end_ff23 <= mode_end_ff22; 
    end
end

assign fragment_gen_end = mode_end_ff23;
assign xy_out_en = xy_out_en_wire && ~busy;
endmodule
