 //-----------------------------------------------------------------------------
//
//Copyright(c) 2020, ThorsianWay Technologies Co, Ltd
//All rights reserved.
//
//IP Name       :   pixel_shader
//File Name     :   clz_27.v
//Module name   :   clz_27
//Full name     :   27 bit unsigned numbei leading zero count
//
//Author        :   zha daolu
//Email         :   
//Data          :   2020/5/13
//Version       :   V1.00
//
//Abstract      :   
//                  
//Called  by    :   GPU
//
//Modification history
//-----------------------------------------------------
//1.00: intial version 
//
//-----------------------------------------------------------------------------                               
module clz_27(
input [26:0] a,
output reg [4:0] clz_o
);

always@(*)
begin
	casex(a)
	27'b1xxxxxxxxxxxxxxxxxxxxxxxxxx: clz_o = 5'd0;
	27'b01xxxxxxxxxxxxxxxxxxxxxxxxx: clz_o = 5'd1;
	27'b001xxxxxxxxxxxxxxxxxxxxxxxx: clz_o = 5'd2;
	27'b0001xxxxxxxxxxxxxxxxxxxxxxx: clz_o = 5'd3;	
	27'b00001xxxxxxxxxxxxxxxxxxxxxx: clz_o = 5'd4;
	27'b000001xxxxxxxxxxxxxxxxxxxxx: clz_o = 5'd5;
	27'b0000001xxxxxxxxxxxxxxxxxxxx: clz_o = 5'd6;
	27'b00000001xxxxxxxxxxxxxxxxxxx: clz_o = 5'd7;
	27'b000000001xxxxxxxxxxxxxxxxxx: clz_o = 5'd8;
	27'b0000000001xxxxxxxxxxxxxxxxx: clz_o = 5'd9;
	27'b00000000001xxxxxxxxxxxxxxxx: clz_o = 5'd10;
	27'b000000000001xxxxxxxxxxxxxxx: clz_o = 5'd11;
	27'b0000000000001xxxxxxxxxxxxxx: clz_o = 5'd12;
	27'b00000000000001xxxxxxxxxxxxx: clz_o = 5'd13;
	27'b000000000000001xxxxxxxxxxxx: clz_o = 5'd14;
	27'b0000000000000001xxxxxxxxxxx: clz_o = 5'd15;
	27'b00000000000000001xxxxxxxxxx: clz_o = 5'd16;
	27'b000000000000000001xxxxxxxxx: clz_o = 5'd17;
	27'b0000000000000000001xxxxxxxx: clz_o = 5'd18;
	27'b00000000000000000001xxxxxxx: clz_o = 5'd19;
	27'b000000000000000000001xxxxxx: clz_o = 5'd20;
	27'b0000000000000000000001xxxxx: clz_o = 5'd21;
	27'b00000000000000000000001xxxx: clz_o = 5'd22;
	27'b000000000000000000000001xxx: clz_o = 5'd23;
	27'b0000000000000000000000001xx: clz_o = 5'd24;
	27'b00000000000000000000000001x: clz_o = 5'd25;
	27'b000000000000000000000000001: clz_o = 5'd26;
	27'b000000000000000000000000000: clz_o = 5'd27;
	default:                         clz_o = 5'd0;	
		
	endcase
end

endmodule
