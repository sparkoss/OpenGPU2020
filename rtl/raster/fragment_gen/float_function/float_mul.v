//-----------------------------------------------------------------------------
//
//Copyright(c) 2020, ThorsianWay Technologies Co, Ltd
//All rights reserved.
//
//IP Name       :   float_mul
//File Name     :   float_mul.v
//Module name   :   float mul
//Full name     :   float number multiplier 
//
//Author        :   zha daolu
//Email         :   
//Data          :   2020/5/13
//Version       :   V1.0
//
//Abstract      :   
//                  
//Called  by    :   GPU
//
//Modification history
//-----------------------------------------------------
//1.00: intial version 
//1.01: add pipeline stall port
//
//-----------------------------------------------------------------------------

//-----------------------------
//DEFINE MACRO
//----------------------------- 
module float_mul(
    input [31:0] a,
    input [31:0] b,
    output [31:0] z
);
	wire [23:0] a_m = {1'b1,a[22 : 0]};
    wire [23:0] b_m = {1'b1,b[22 : 0]};
    wire  [7:0] a_e = a[30 : 23] - 127;
    wire  [7:0] b_e = b[30 : 23] - 127;
    wire   		a_s = a[31];
    wire   		b_s = b[31];
	
	wire z_sign = a_s ^ b_s;
	wire [47:0] product = a_m * b_m;
	wire [22:0] z_m = product[47] ? product[46:24] : product[45:23];
	wire [7:0] z_e = (product[47] ? $signed(a_e) + $signed(b_e) + 1 : $signed(a_e) + $signed(b_e)) + 127;
	
	assign z = {z_sign,z_e,z_m};

endmodule
