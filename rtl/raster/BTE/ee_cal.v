//-----------------------------------------------------------------------------
//
//Copyright(c) 2020, ThorsianWay Technologies Co, Ltd
//All rights reserved.
//
//IP Name       :   raster
//File Name     :   ee_cal.v
//Module name   :   ee_cal
//Full name     :   calculate edge function for a pixel
//
//Author        :   zha daolu
//Email         :   
//Data          :   2020/6/1
//Version       :   V1.00
//
//Abstract      :   
//                  
//Called  by    :   GPU
//
//Modification history
//-----------------------------------------------------
//1.00: intial version 
//
//-----------------------------------------------------------------------------

//-----------------------------
//DEFINE MACRO
//-----------------------------   

module ee_cal
(
    input                   clk,            //input clock
    input                   rst_n,          //input reset, low active
    input                   busy,           //input next stage busy
    input signed[47:0]      a,              //edge function parameter 1.31.16
    input signed[47:0]      b,              //edge function parameter 1.31.16
    input signed[95:0]      ee_int,         //edge function value for tile left-bottom 1.47.32
    input[3:0]              x,              //in-tile x coordinate
    input[3:0]              y,              //in-tile y coordinate
    output reg signed[95:0] ee_out          //output edge function 1.47.32
);

wire signed[53:0]               mult_out0;
wire signed[53:0]               mult_out1;

reg signed[53:0]                mult_out0_reg;
reg signed[53:0]                mult_out1_reg;


mult_signed_5x48    uut_mult0
(
    .a  ($signed({1'b0,x})),
    .b  (a),
    .out(mult_out0)
);

mult_signed_5x48    uut_mult1
(
    .a  ($signed({1'b0,y})),
    .b  (b),
    .out(mult_out1)
);


always@(posedge clk or negedge rst_n)
begin
    if(!rst_n)
        begin
            mult_out0_reg <= 54'b0;
            mult_out1_reg <= 54'b0;    
        end
    else if(!busy)
        begin
            mult_out0_reg <= mult_out0;
            mult_out1_reg <= mult_out1;
        end
end


//ee = ee_int + ax + by
wire signed[95:0]   ee = ee_int + (mult_out0_reg<<<16) + (mult_out1_reg<<<16);


always@(posedge clk or negedge rst_n)
begin
    if(!rst_n)
        ee_out  <= 96'b0;
    else if(!busy)
        ee_out  <= ee;
end
endmodule











