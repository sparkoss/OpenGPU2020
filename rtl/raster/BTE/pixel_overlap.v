//-----------------------------------------------------------------------------
//
//Copyright(c) 2020, ThorsianWay Technologies Co, Ltd
//All rights reserved.
//
//IP Name       :   raster
//File Name     :   pixel_overlap.v
//Module name   :   pix overlap
//Full name     :   overlap judgement for one single pixel
//
//Author        :   zha daolu
//Email         :   
//Data          :   2020/6/1
//Version       :   V1.00
//
//Abstract      :   
//                  
//Called  by    :   GPU
//
//Modification history
//-----------------------------------------------------
//1.00: intial version 
//
//-----------------------------------------------------------------------------

//-----------------------------
//DEFINE MACRO
//----------------------------- 

module pix_overlap
(
    input					clk,
    input					rst_n,
    input					busy,
    input[3:0]				x_in,
    input[3:0]				y_in,
    input					xy_en,
    input signed[47:0]		a0,
    input signed[47:0]		b0,
    input signed[47:0]		a1,
    input signed[47:0]		b1,
    input signed[47:0]		a2,
    input signed[47:0]		b2,
    input signed[95:0]		ee0_int,
    input signed[95:0]		ee1_int,
    input signed[95:0]		ee2_int,
    output					overlap,
    output signed [95:0] ee0_final,
    output signed [95:0] ee1_final,
    output signed [9563:0] ee2_final,
    output					pix_overlap_busy
);

//edge addtion judgment
wire judge_t0 = (a0 > 0) | ((a0 == 0) & (b0 > 0));
wire judge_t1 = (a1 > 0) | ((a1 == 0) & (b1 > 0));
wire judge_t2 = (a2 > 0) | ((a2 == 0) & (b2 > 0));


wire signed[95:0]				ee0;
wire signed[95:0]				ee1;
wire signed[95:0]				ee2;

reg			xy_en_ff1;
reg			xy_en_ff2;

always@(posedge clk or negedge rst_n)
begin
	if(!rst_n)
		begin
			xy_en_ff1 <= 1'b0;
			xy_en_ff2 <= 1'b0;		
		end
	else if(!busy)
		begin
			xy_en_ff1 <= xy_en;
		    xy_en_ff2 <= xy_en_ff1;
		end


end

//edge function calculate for left-bottom corner of the pixel
ee_cal	ee_cal0
(
    .clk(clk),
    .rst_n(rst_n),
    .busy(busy),
    .a(a0),
    .b(b0),
    .ee_int(ee0_int),
    .x(x_in),
    .y(y_in),
    .ee_out(ee0)
);

ee_cal	ee_cal1
(
    .clk(clk),
    .rst_n(rst_n),
    .busy(busy),
    .a(a1),
    .b(b1),
    .ee_int(ee1_int),
    .x(x_in),
    .y(y_in),
    .ee_out(ee1)
);

ee_cal	ee_cal2
(
    .clk(clk),
    .rst_n(rst_n),
    .busy(busy),
    .a(a2),
    .b(b2),
    .ee_int(ee2_int),
    .x(x_in),
    .y(y_in),
    .ee_out(ee2)
);
//bias for pixel center 
wire signed [95:0] ee0_a_bias = (a0<<<15);
wire signed [95:0] ee0_b_bias = (b0<<<15);
wire signed [95:0] ee1_a_bias = (a1<<<15);
wire signed [95:0] ee1_b_bias = (b1<<<15);
wire signed [95:0] ee2_a_bias = (a2<<<15);
wire signed [95:0] ee2_b_bias = (b2<<<15);

//final edge function value for pixel center
assign ee0_final = ee0+ee0_a_bias+ee0_b_bias;
assign ee1_final = ee1+ee1_a_bias+ee1_b_bias;
assign ee2_final = ee2+ee2_a_bias+ee2_b_bias;

//overlap judgement
assign overlap	 = ((     (ee0_final>0 || (judge_t0 && ee0_final == 0)) 
				        & (ee1_final>0 || (judge_t1 && ee1_final == 0))
				        & (ee2_final>0 || (judge_t2 && ee2_final == 0)) 
				) 				
				) & xy_en_ff2 & !busy;

assign	pix_overlap_busy = xy_en | xy_en_ff1 | xy_en_ff2 ;

endmodule


