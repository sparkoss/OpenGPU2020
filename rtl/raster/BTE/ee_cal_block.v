//-----------------------------------------------------------------------------
//
//Copyright(c) 2020, ThorsianWay Technologies Co, Ltd
//All rights reserved.
//
//IP Name       :   raster
//File Name     :   ee_cal_block.v
//Module name   :   ee_cal_block
//Full name     :   calculate ee for block
//
//Author        :   zha daolu
//Email         :   
//Data          :   2020/6/1
//Version       :   V1.00
//
//Abstract      :   
//                  
//Called  by    :   GPU
//
//Modification history
//-----------------------------------------------------
//1.00: intial version 
//
//-----------------------------------------------------------------------------

//-----------------------------
//DEFINE MACRO
//-----------------------------

module ee_cal_block
(
    input					clk,            //input clock
    input					rst_n,          //input reset, active low
    input					busy,           //input next stage busy
    input signed[47:0]		a,              //ee parameter, 1.31.16
    input signed[47:0]		b,              //ee parameter, 1.31.16
    input signed[95:0]		ee_int,			//ee for tile left-bottom corner, 1.63.32
    input[4:0]				x,              //input x coordinate in tile
    input[4:0]				y,              //input y coordinate in tile
    output reg				sign			//output ee sign bit
);

wire signed[53:0]				mult_out0;
wire signed[53:0]				mult_out1;

reg signed[53:0]				mult_out0_reg;
reg signed[53:0]				mult_out1_reg;

//ee = ee_int + ax + by
mult_signed_5x48	uut_mult0
(
    .a  ($signed({1'b0,x})),
    .b  (a),
    .out(mult_out0)
);

mult_signed_5x48	uut_mult1
(
    .a  ($signed({1'b0,y})),
    .b  (b),
    .out(mult_out1)
);


always@(posedge clk or negedge rst_n)
begin
	if(!rst_n)
		begin
			mult_out0_reg <=  54'b0;
			mult_out1_reg <=  54'b0;	
		end
	else if(!busy)
		begin
			mult_out0_reg <=  mult_out0;
			mult_out1_reg <=  mult_out1;
		end
end

wire signed[95:0]	ee = ee_int + (mult_out0_reg<<<16) + (mult_out1_reg<<<16);


always@(*)
begin
	//if(!rst_n)
	//	sign  <= `DLY 0;
	//else if(!busy)
		sign	= ee[95];
end
endmodule











