`timescale 1ns/1ps 

module bte_tb;

reg clk;
reg rst_n;
wire   	 busy;
wire   	 bte_start;
wire [31:0] tile_x;
wire [31:0] tile_y;                             
wire [47:0] vertex0_x               ;
wire [47:0] vertex0_y               ; 
wire [47:0] vertex1_x               ;
wire [47:0] vertex1_y               ;
wire [47:0] vertex2_x               ;
wire [47:0] vertex2_y               ;
wire [31:0]					x_out;
wire [31:0]					y_out;
wire  						xy_out_en;
wire [95:0]      ee0;
wire [95:0]      ee1;
wire [95:0]      ee2;
wire bte_busy;
wire test_end;

BTE uut_bte(
    .clk(clk),
    .rst_n(rst_n),
    .bte_start(bte_start),
    .busy(busy),
    .tile_x(tile_x),
    .tile_y(tile_y),
    .vertex0_x(vertex0_x),
    .vertex0_y(vertex0_y),
    .vertex1_x(vertex1_x),
    .vertex1_y(vertex1_y),
    .vertex2_x(vertex2_x),
    .vertex2_y(vertex2_y),
    .vertex0_z(vertex0_y),
    .vertex1_z(vertex1_y),
    .vertex2_z(vertex2_y),
    .x_out(x_out),
    .y_out(y_out),
    .xy_out_en(xy_out_en),
    .ee0(ee0),
    .ee1(ee1),
    .ee2(ee2),
    .bte_busy(bte_busy)
);

bte_checker(
    .clk(clk),
    .rst_n(rst_n),
    .busy(busy),
    .tile_x(tile_x),
    .tile_y(tile_y),
    .vertex0_x(vertex0_x),
    .vertex0_y(vertex0_y),
    .vertex1_x(vertex1_x),
    .vertex1_y(vertex1_y),
    .vertex2_x(vertex2_x),
    .vertex2_y(vertex2_y),
    .x_out(x_out),
    .y_out(y_out),
    .test_end(test_end),
    .xy_out_en(xy_out_en),
    .ee0(ee0),
    .ee1(ee1),
    .ee2(ee2),
    .bte_start(bte_start),
    .bte_busy(bte_busy) 
        ); 

always #5 clk = ~clk;

initial
begin
   clk = 0;
   rst_n = 0;
   #100 @(posedge clk) rst_n = 1;
   wait(test_end && ~bte_busy)
   #1000 $stop;
end

initial
begin
    
	$fsdbDumpfile("bte_wave.fsdb");
	$fsdbDumpvars(0,bte_tb);
end  
endmodule
